#pragma once

#include "IDatabase.h"
#include "sqlite3.h"
#include <io.h>
#include <string>
#include <mutex>

class SqliteDatabase : public IDatabase
{
private:
	sqlite3* _db;
	static int callbackResult(void* data, int argc, char** argv, char** azColname);
	static int callbackQuestions(void* data, int argc, char** argv, char** azColname);
	static int callbackStrings(void* data, int argc, char** argv, char** azColname);
public:
	SqliteDatabase();
	~SqliteDatabase();
	virtual bool doesUserExist(std::string username);
	virtual bool doesPasswordMatch(std::string username, std::string password);
	virtual void addNewUser(std::string username, std::string password, std::string email);
	virtual std::vector<Question> getQuestions(int numOfQuestion);
	virtual float getPlayerAverageAnswerTime(std::string username);
	virtual int getNumOfCorrectAnswers(std::string username);
	virtual int getNumOfTotalAnswers(std::string username);
	virtual int getNumOfPlayerGames(std::string username);
	virtual void changeStatistics(float newAverageAnswerTime, int newNumOfCorrectAnswers, int newNumOfTotalAnswers, std::string user);
	virtual std::vector<std::string> getAllUsers();
	
};
