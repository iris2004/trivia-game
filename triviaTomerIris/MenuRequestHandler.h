#pragma once

#include "IRequestHandler.h"
#include "LoggedUser.h"
#include "StatisticsManager.h"
#include "RoomManager.h"
#include "RoomAdminRequestHandler.h"
#include "RoomMemberRequestHandler.h"

#define JOIN_ROOM_REQUEST_CODE '4'
#define CREATE_ROOM_REQUEST_CODE '5'
#define LOGOUT_CODE '6'
#define GET_ROOMS_REQUEST_CODE '7'
#define GET_PLAYERS_IN_ROOM_REQUEST_CODE '8'
#define GET_HIGH_SCORE '9'
#define GET_STATISTICS_REQUEST_CODE 'A' 

class RequestHandleFactory;

///class responsible to the requests that are menu relates
class MenuRequestHandler : public IRequestHandler
{
private:
    LoggedUser m_user;
    RoomManager& m_roomManager;
    StatisticsManager& m_statisticsManager;
    RequestHandleFactory* m_requestHandleFactory;

public:
    MenuRequestHandler(RequestHandleFactory* requestHandleFactory);
    virtual	bool isRequestRelevant(RequestInfo reqIn);
    virtual RequestResult handleRequest(RequestInfo reqIn);
    void setRoomManager(RoomManager rm);
};