#include "MenuRequestHandler.h"
#include "RequestHandleFactory.h"

/*
C'tor to the MenuRequestHandler class
input: requestHandleFactory pointer
output: none
*/
MenuRequestHandler::MenuRequestHandler(RequestHandleFactory* requestHandleFactory) :
    m_user{ requestHandleFactory->getUser() },
    m_requestHandleFactory{ requestHandleFactory },
    m_statisticsManager{ requestHandleFactory->getStatisticsManager() },
    m_roomManager { requestHandleFactory->getRoomManager() }
{
  
}

/*
method check if the request is relevant to the handler
input: info of the request
output: true - if the request is relevant
        false - if the request is irelevant
*/
bool MenuRequestHandler::isRequestRelevant(RequestInfo reqIn)
{
    if ((reqIn.id >= JOIN_ROOM_REQUEST_CODE && reqIn.id <= GET_HIGH_SCORE) || reqIn.id == GET_STATISTICS_REQUEST_CODE)
    {
        return true;
    }	
    return false;
}

/*
method handle a new request from the client
input: info of the request
output: result of the requests
*/
RequestResult MenuRequestHandler::handleRequest(RequestInfo reqIn)
{
    RequestResult reqRes;
    try {       
        if (!isRequestRelevant(reqIn)) throw(std::exception("request is irelevant"));
        if (reqIn.id == JOIN_ROOM_REQUEST_CODE)
        {
            JoinRoomRequest jrq = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(reqIn.buffer);
            std::vector<std::string> users = m_roomManager.getRoom(jrq.roomId).getAllUsers();

            if (std::find(users.begin(), users.end(), m_user.getUsername()) != users.end())
            {
                throw std::exception("user already in the room");
            }
            else
            {
                m_roomManager.getRoom(jrq.roomId).addUser(m_user.getUsername());
            }
            JoinRoomResponse jrr;
            jrr.status = 1;
            reqRes.response = JsonResponsePacketSerializer::serializeResponse(jrr);
            reqRes.newHandler = m_requestHandleFactory->createRoomMemberRequestHandler(m_roomManager.getRoom(jrq.roomId));
        }
        else if (reqIn.id == CREATE_ROOM_REQUEST_CODE)
        {
            CreateRoomRequest crq = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(reqIn.buffer);
            RoomData rd;
            rd.id = 0;
            rd.maxPlayer = crq.maxUsers;
            rd.name = crq.roomName;
            rd.numOfQuestionInGame = crq.questionCount;
            rd.timePerQuestion = crq.answerTimeOut;
            rd.isActive = 1;
            m_roomManager.createRoom(m_user, rd);
            CreatRoomResponse crr;
            crr.status = 1;
            reqRes.response = JsonResponsePacketSerializer::serializeResponse(crr);
            reqRes.newHandler = m_requestHandleFactory->createRoomAdminRequestHandler(m_roomManager.getRoom(rd.id));
        }
        else if (reqIn.id == LOGOUT_CODE)
        {
            m_requestHandleFactory->getLoginManager().logout(m_user.getUsername());
            LogoutResponse lr;
            lr.status = 1;
            reqRes.response = JsonResponsePacketSerializer::serializeResponse(lr);
            reqRes.newHandler = new LoginRequestHandler(&m_requestHandleFactory->getLoginManager(), m_requestHandleFactory);
        }
        else if (reqIn.id == GET_ROOMS_REQUEST_CODE)
        {
            GetRoomsResponse grr;
            grr.rooms = m_roomManager.getRooms();
            grr.status = 1;
            reqRes.response = JsonResponsePacketSerializer::serializeResponse(grr);
            reqRes.newHandler = new MenuRequestHandler(m_requestHandleFactory);
        }
        else if (reqIn.id == GET_PLAYERS_IN_ROOM_REQUEST_CODE)
        {
            GetPlayersInRoomRequest gp = JsonRequestPacketDeserializer::deserializeGetPlayersRequest(reqIn.buffer);
            GetPlayersInRoomResponse gpr;
            gpr.players = m_roomManager.getRoom(gp.roomId).getAllUsers();
            reqRes.response = JsonResponsePacketSerializer::serializeResponse(gpr);
            reqRes.newHandler = new MenuRequestHandler(m_requestHandleFactory);
        }
        else if (reqIn.id == GET_HIGH_SCORE)
        {
            GetHighScoreResponse ghsr;
            ghsr.statistics = m_statisticsManager.getHighScore();
            ghsr.status = 1;
            reqRes.response = JsonResponsePacketSerializer::serializeResponse(ghsr);
            reqRes.newHandler = new MenuRequestHandler(m_requestHandleFactory);
        }
        else if (reqIn.id == GET_STATISTICS_REQUEST_CODE)
        {
            GetPersonalStatsResponse gprs;
            gprs.statistics = m_statisticsManager.getUserStatistics(m_user.getUsername());
            reqRes.response = JsonResponsePacketSerializer::serializeResponse(gprs);
            reqRes.newHandler = new MenuRequestHandler(m_requestHandleFactory);
        }
    }
    catch (std::exception &s)
    {
        ErrorResponse res;
        res.message = s.what();
        reqRes.response = JsonResponsePacketSerializer::serializeResponse(res);
        reqRes.newHandler = this;
        std::cout << s.what() << std::endl;
    }
    return reqRes;
}

/*
setter to the room manager
input: a roomManager
output: none
*/
void MenuRequestHandler::setRoomManager(RoomManager rm)
{
    m_roomManager = rm;
}
