#pragma once
#include <string>
#include "LoggedUser.h"
#include <vector>

struct RoomData
{
	unsigned int id;
	std::string name;
	unsigned int maxPlayer;
	unsigned numOfQuestionInGame;
	unsigned int timePerQuestion;
	unsigned int isActive;
};

//the class represent a room
class Room
{
private:
	RoomData m_metadata;
	std::vector<LoggedUser> m_users;
public:
	Room();
	Room(RoomData& rd, LoggedUser user);
	void addUser(LoggedUser user);
	void removeUser(LoggedUser user);
	std::vector<std::string> getAllUsers();
	RoomData& getRoomData();
	void changeRoomState(unsigned int state);
};