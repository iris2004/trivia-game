#include "Question.h"

/*
getter to the question field
input: none
output: the question
*/
std::string Question::getQuestion()
{
	return this->m_question;
}

/*
getter to the possible answers
input: none
output: vector of possible answers
*/
std::vector<std::string> Question::getPossibleAnswers()
{
	return this->m_possibleAnswers;
}

/*
getter to the correct answers
input: none
output: the id of the correct question
*/
int Question::getCorrentAnswer()
{
	return this->correctAnswer;
}

/*
setter to the question
input: a question 
output: none
*/
void Question::setQuestion(std::string question)
{ 
	this->m_question = question;
}

/*
setter to the possible answers
input: answers
output: none
*/
void Question::setAnswers(std::vector<std::string> answers)
{
	this->m_possibleAnswers = answers;
}

/*
setter to the correct answer
input: correct answer id
output: none
*/
void Question::setCorrectAnswer(int corrrectAns)
{
	this->correctAnswer = corrrectAns;
}
