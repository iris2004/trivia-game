#include "GameRequestHandler.h"

/*
method gets the next question
input: info of the rquest
output: results of the request
*/
RequestResult GameRequestHandler::getQuestion(RequestInfo reqIn)
{
    RequestResult reqRes;
    GetQuestionResponse res;
    m_game.getQuestionForUser(m_user.getUsername());
    std::vector<std::string> answers = m_game.getPlayers()[m_user.getUsername()].currentQuestion.getPossibleAnswers();
    res.status = 1;
    res.question = m_game.getPlayers()[m_user.getUsername()].currentQuestion.getQuestion();
    for (unsigned int i = 0; i < NUM_OF_POSSIBLE_ANSWERS; i++)
    {
        res.answers[i] = answers[i];
    }   
    reqRes.response = JsonResponsePacketSerializer::serializeResponse(res);
    reqRes.newHandler = this;
    return reqRes;
}

/*
method submit the answer of a user
input: info of the rquest
output: results of the request
*/
RequestResult GameRequestHandler::submitAnswer(RequestInfo reqIn)
{
    RequestResult reqRes;
    SubmitAnswerRequest req;
    SubmitAnswersResponse res;

    req = JsonRequestPacketDeserializer::deserializerSubmitAnswerRequest(reqIn.buffer);
    m_game.submitAnswer(m_user.getUsername(), req.answerid, req.answerTime);
    res.status = 1;
    res.correctAnswerId = m_game.getPlayers()[m_user.getUsername()].currentQuestion.getCorrentAnswer();
    reqRes.response = JsonResponsePacketSerializer::serializeResponse(res);
    reqRes.newHandler = this;
    return reqRes;
}

/*
method get the result at the end of a game
input: info of the rquest
output: results of the request
*/
RequestResult GameRequestHandler::getGameResults(RequestInfo reqIn)
{
    RequestResult reqRes;
    GetGameResultResponse res;
    PlayerResultResponse currPlayer;
    GameData user = m_game.getPlayers().find(m_user.getUsername())->second;

    //hange the statistics at the db
    m_gameManager.getDataBase()->changeStatistics(user.averageAnswerTime, user.correctAnswerCount, user.correctAnswerCount + user.wrongAnswerCount, m_user.getUsername());
    res.status = 1;
    std::map<std::string, GameData> players = m_game.getPlayers();
    std::map<std::string, GameData>::iterator i;
    //aplly the result of every user
    for (i = players.begin(); i != players.end(); i++)
    {
        currPlayer.AverageAnswerTime = i->second.averageAnswerTime;
        currPlayer.correctAnswerCount = i->second.correctAnswerCount;
        currPlayer.wrongtAnswerCount = i->second.wrongAnswerCount;
        currPlayer.userName = i->first;
        res.results.push_back(currPlayer);        
    }
    this->m_handlerFacroty.getRoomManager().getRoom(m_game.getId()).removeUser(m_user.getUsername());
    if (this->m_handlerFacroty.getRoomManager().getRoom(m_game.getId()).getAllUsers().size() == 0)  this->m_handlerFacroty.getRoomManager().deleteRoom(m_game.getId());
    reqRes.response = JsonResponsePacketSerializer::serializeResponse(res);
    reqRes.newHandler = m_handlerFacroty.createMenueHandler();
    return reqRes;
}

/*
method remove a user from a game
input: info of the rquest
output: results of the request
*/
RequestResult GameRequestHandler::leaveGame(RequestInfo reqIn)
{
    RequestResult reqRes;
    LeaveGameResponse res;

    m_game.removePlayer(m_user.getUsername());
    m_handlerFacroty.getRoomManager().getRoom(m_game.getId()).removeUser(m_user);
    if (m_game.getPlayers().size() == 0) m_gameManager.deleteGame(m_game);

    res.status = 1;
    reqRes.response = JsonResponsePacketSerializer::serializeResponse(res);
    reqRes.newHandler = m_handlerFacroty.createMenueHandler();
    return reqRes;
}

/*
C'tor to the GameRequestHandler
input: RequestHandleFactory and a room
output: none
*/
GameRequestHandler::GameRequestHandler(RequestHandleFactory* handlerFacroty, Room& room): m_handlerFacroty(*handlerFacroty), m_gameManager(handlerFacroty->getGameManager()), m_game(m_gameManager.createGame(room))
{
    this->m_user = LoggedUser(handlerFacroty->getUser());
}

/*
method check if the request is relevant to the handler
input: info of the rquest
output: results of the request
*/
bool GameRequestHandler::isRequestRelevant(RequestInfo reqIn)
{
    if (reqIn.id == LEAVE_GAME_REQUEST_CODE || reqIn.id == GET_GAME_RESULT_REQUEST_CODE || reqIn.id == SUBMIT_ANSWER_REQUEST_CODE || reqIn.id == GET_QUESTION_REQUEST_CODE)
    {
        return true;
    }
    return false;
}

/*
methos handle with the reqest the client sent
input: info of the rquest
output: results of the request
*/
RequestResult GameRequestHandler::handleRequest(RequestInfo reqIn)
{
    RequestResult reqRes;
    try {
        if (!isRequestRelevant(reqIn)) throw(std::exception("request is irelevant"));
        if (reqIn.id == LEAVE_GAME_REQUEST_CODE)
        {
            reqRes = leaveGame(reqIn);
        }
        else if (reqIn.id == GET_GAME_RESULT_REQUEST_CODE)
        {
            reqRes = getGameResults(reqIn);
        }
        else if (reqIn.id == SUBMIT_ANSWER_REQUEST_CODE)
        {
            reqRes = submitAnswer(reqIn);
        }
        else if (reqIn.id == GET_QUESTION_REQUEST_CODE)
        {
            reqRes = getQuestion(reqIn);
        }
    }
    catch (std::exception &s)
    {
        ErrorResponse res;
        res.message = s.what();
        reqRes.response = JsonResponsePacketSerializer::serializeResponse(res);
        reqRes.newHandler = this;
    }
    return reqRes;
}