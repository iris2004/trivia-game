#include "GameManager.h"

IDatabase* GameManager::m_database = new SqliteDatabase();
std::vector<Game> GameManager::m_games;

/*
method create a new game according to the room
input: a room
output: a new game
*/
Game GameManager::createGame(Room room)
{
    GameData gd;
    std::map<std::string, GameData> m;
    std::vector<Question> q = m_database->getQuestions(room.getRoomData().numOfQuestionInGame);
    std::vector<std::string> users = room.getAllUsers();

    gd.averageAnswerTime = 0;
    gd.correctAnswerCount = 0;
    gd.currentQuestion = q[0];
    gd.wrongAnswerCount = 0;
    for (int i = 0; i < users.size(); i++)
    {
        m.insert(std::pair<std::string, GameData>(users[i], gd)); //insert all the plyers to the game
    }
    int id = room.getRoomData().id;
    return Game(q, m, id);
}

/*
method delete a game
input: game to delete
output: none
*/
void GameManager::deleteGame(Game game)
{
    bool check = false;
    std::vector<Game>::iterator it;
    for (it = m_games.begin(); it != m_games.end() && !check; it++) //search the game
    {
        if (game.getId() == it->getId())
        {
            m_games.erase(it);
            check = true;
        }
    }
    if (!check) throw std::exception("game does not exist");
}

/*
getter to the databse
input: none
output: the data base
*/
IDatabase* GameManager::getDataBase()
{
    return this->m_database;
}
