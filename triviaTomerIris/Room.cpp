#include "Room.h"
/// <summary>
/// empty ctor
/// </summary>
Room::Room()
{
}
/// <summary>
/// ctor that uses a RoomData struct and a logged user object
/// </summary>
/// <param name="rd">the information of the room</param>
/// <param name="user">the user that creats the room</param>
Room::Room(RoomData& rd, LoggedUser user)
{
	m_metadata = rd;
	addUser(user);
}
/// <summary>
/// adds a user to the room
/// </summary>
/// <param name="user">the user that is added to the room</param>
void Room::addUser(LoggedUser user)
{
	if (this->m_users.size() == this->m_metadata.maxPlayer) throw std::exception("room is already full");
	m_users.push_back(user);
}
/// <summary>
/// removes a user in the 
/// </summary>
/// <param name="user"></param>
void Room::removeUser(LoggedUser user)
{
	bool flag = false;
	std::vector<LoggedUser>::iterator it = m_users.begin();
	for (it; it != m_users.end(); it++)
	{
		if (it->getUsername() == user.getUsername())
		{
			m_users.erase(it);
			flag = true;
			break;
			
		}
	}
}

/*
method get all the users in the room
input: none
output: vector of the users
*/
std::vector<std::string> Room::getAllUsers()
{
	std::vector<std::string> userVec;
	for (int i = 0; i < m_users.size(); i++)
	{
		userVec.push_back(m_users[i].getUsername());
	}
	return userVec;
}

/*
getter to the room data
input: none
output: room data
*/
RoomData& Room::getRoomData()
{
	return m_metadata;
}

/*
setter to the room state
input: room state
output: none
*/
void Room::changeRoomState(unsigned int state)
{
	m_metadata.isActive = state;
}

