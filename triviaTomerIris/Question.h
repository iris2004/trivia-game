#pragma once

#include <iostream>
#include <vector>

//the class represent a question
class Question
{
private:
	std::string m_question;
	std::vector<std::string> m_possibleAnswers;
	int correctAnswer; //the id of the correct answer

public:
	std::string getQuestion();
	std::vector<std::string> getPossibleAnswers();
	int getCorrentAnswer();
	void setQuestion(std::string question);
	void setAnswers(std::vector<std::string> answers);
	void setCorrectAnswer(int corrrectAns);
};