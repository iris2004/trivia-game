#include "RoomAdminRequestHandler.h"

/*
method close the room
input: none
output: results of the request
*/
RequestResult RoomAdminRequestHandler::closeRoom()
{
	RequestResult reqRes;
	CloseRoomResponse res;
	m_room.changeRoomState(ROOM_CLOSED);
	if (m_room.getAllUsers().size() == 1)
	{
		m_roomManager.deleteRoom(m_room.getRoomData().id);
	}
	res.status = 1;
	reqRes.response = JsonResponsePacketSerializer::serializeResponse(res);
	reqRes.newHandler = new MenuRequestHandler(&m_handlerFactory);
	return reqRes;
}

/*
C'tor to the class
*/
RoomAdminRequestHandler::RoomAdminRequestHandler(RequestHandleFactory* requestHandleFactory, Room& room) : IRequestHandler(), m_handlerFactory(*requestHandleFactory), m_roomManager(requestHandleFactory->getRoomManager()),m_room(room)
{
	m_user = m_handlerFactory.getUser();
}

/*
method check if the request is relevant to the handler
input: info of the request
outputL: true/ false
*/
bool RoomAdminRequestHandler::isRequestRelevant(RequestInfo req)
{
	if (req.id == CLOSE_ROOM_REQUEST_CODE || req.id == START_GAME_REQUEST_CODE || req.id == GET_ROOM_STATE_REQUEST_CODE)
	{
		return true;
	}
	return false;
}

/*
method handle the request
input: info of the request
output: result of the request
*/
RequestResult RoomAdminRequestHandler::handleRequest(RequestInfo req)
{
	RequestResult reqRes;
	try {
		if (!isRequestRelevant(req)) throw std::exception("request is irelevant");
		if (req.id == CLOSE_ROOM_REQUEST_CODE)
		{
			reqRes = closeRoom();
		}
		else if (req.id == START_GAME_REQUEST_CODE)
		{
			reqRes = PlayerInRoomRequestHandler::startGame(m_room);
			reqRes.newHandler = m_handlerFactory.createGameRequestHandler(m_room);
			m_room.changeRoomState(2);
		}
		else if (req.id == GET_ROOM_STATE_REQUEST_CODE)
		{
			reqRes = PlayerInRoomRequestHandler::getRoomState(m_room);
			reqRes.newHandler = this;
		}
	}
	catch (std::exception& s)
	{
		ErrorResponse res;
		res.message = s.what();
		reqRes.response = JsonResponsePacketSerializer::serializeResponse(res);
		reqRes.newHandler = this;
		std::cout << s.what() << std::endl;
	}
	return reqRes;
}
