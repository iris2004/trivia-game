#pragma once
#include "json.hpp"
#include "IRequestHandler.h"
#include <vector>
using json = nlohmann::json;

struct LoginRequest {
	std::string username;
	std::string password;
};

struct SignupRequest{
	std::string username;
	std::string password;
	std::string email;
};

struct GetPlayersInRoomRequest {
	unsigned int roomId;
};

struct JoinRoomRequest {
	unsigned int roomId;
};

struct CreateRoomRequest {
	String roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeOut;
};

struct SubmitAnswerRequest {
	unsigned int answerid;
	float answerTime;
};

//class responsible to deserialize all the messages that the clients send
class JsonRequestPacketDeserializer
{
public:
	static LoginRequest deserializeLoginRequest (BufferChar buffer);
	static SignupRequest deserializeSignupRequest (BufferChar buffer);
	static GetPlayersInRoomRequest deserializeGetPlayersRequest (BufferChar buffer);
	static JoinRoomRequest deserializeJoinRoomRequest (BufferChar buffer);
	static CreateRoomRequest deserializeCreateRoomRequest (BufferChar buffer); 
	static SubmitAnswerRequest deserializerSubmitAnswerRequest(BufferChar buffer);
};

