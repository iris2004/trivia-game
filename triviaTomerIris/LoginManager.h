#pragma once

#include "SqliteDataBase.h"
#include "LoggedUser.h"
#include <vector>
#include <mutex>

//class manage all the login and sign up process
class LoginManager
{
private:
	static IDatabase* m_database;
	static std::vector<LoggedUser> m_loggedUsers; 	
public:
	LoginManager();
	void signup(std::string username, std::string password, std::string email);
	void login(std::string username, std::string password);
	void logout(std::string username);
	IDatabase* getDataBase();
};