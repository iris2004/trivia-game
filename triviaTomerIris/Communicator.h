#pragma once
#define _CRT_SECURE_NO_DEPRECATE
#define _CRT_SECURE_NO_WARNINGS
#include <WinSock2.h>
#include <Windows.h>
#include <thread>
#include <map>
#include "iRequestHandler.h"
#include <iostream>
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include "LoginRequestHandler.h"

#define SERVER_PORT 8876

//the class is responsible to comunicate with the clients
class Communicator
{
private:
	SOCKET m_serverSocket; 
	std::map<SOCKET, IRequestHandler*> m_clients; //the client tha are currently connected
	void bindAndListen();
	void handleNewClient(SOCKET sock);
public:
	void startHandleRequests();
	Communicator();
	~Communicator();
};

