#pragma once

#include "PlayerInRoomRequestHandler.h"
#include "JsonResponsePacketSerializer.h"

class RequestHandleFactory;
class PlayerInRoomRequestHandler;

//class represent an admin of a room
class RoomAdminRequestHandler : public IRequestHandler
{
private:
	Room& m_room;
	LoggedUser m_user;
	RoomManager& m_roomManager;
	RequestHandleFactory& m_handlerFactory;

	RequestResult closeRoom();
public:
	RoomAdminRequestHandler(RequestHandleFactory* requestHandleFactory, Room& room);
	virtual bool isRequestRelevant(RequestInfo req);
	virtual RequestResult handleRequest(RequestInfo req);
};