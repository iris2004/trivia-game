#pragma once
#include "SqliteDataBase.h"
#include "LoginRequestHandler.h"
#include "RoomManager.h"
#include "GameManager.h"
#include "StatisticsManager.h"
#include "GameRequestHandler.h"

class MenuRequestHandler;
class LoginRequestHandler;
class RoomAdminRequestHandler;
class RoomMemberRequestHandler;
class GameRequestHandler;

//class manage all the handlers
class RequestHandleFactory
{
private:
	IDatabase* m_database;
	LoginManager m_loginManager;
	RoomManager& m_roomManager;
	StatisticsManager m_statisticsManager;
	GameManager m_gameManager;
	std::string m_user;
	
public:
	RequestHandleFactory();
	LoginRequestHandler* createLoginRequestHandler();
	LoginManager& getLoginManager();
	MenuRequestHandler* createMenueHandler();
	StatisticsManager& getStatisticsManager();
	RoomManager& getRoomManager();
	RoomAdminRequestHandler* createRoomAdminRequestHandler(Room& room);
	RoomMemberRequestHandler* createRoomMemberRequestHandler(Room& room);
	GameRequestHandler* createGameRequestHandler(Room& room);
	GameManager& getGameManager();
	void setUser(std::string user);
	std::string getUser();
};
