#include "RoomManager.h"

 std::map<unsigned int, Room>* RoomManager::m_rooms = new std::map<unsigned int, Room>();
 std::mutex kys2;
 std::unique_lock<std::mutex> lck2(kys2,std::defer_lock);


RoomManager::RoomManager()
{
	
}

/*
method create a new room
input: a user, room data
output: none
*/
void RoomManager::createRoom(LoggedUser user, RoomData& rd)
{
	std::map<unsigned int, Room>::iterator i;
	bool check = true;
	lck2.lock();
	for (i = m_rooms->begin(); i != m_rooms->end(); i++)
	{
		if (i->second.getRoomData().name == rd.name) check = false;
	}
	if (check != false)
	{
		rd.id = m_rooms->size();
		m_rooms->insert(std::pair<unsigned int, Room>(rd.id, Room(rd, user)));
	}	
	lck2.unlock();
	if (check == false) throw std::exception("name is already taken");
}

/*
method delete a room
input: id of a room
output: noone
*/
void RoomManager::deleteRoom(int ID)
{
	std::unique_lock<std::mutex> lck2(kys2);
	std::map<unsigned int, Room>::iterator it = m_rooms->find(ID);
	m_rooms->erase(it);
	
}

/*
method get a room satate
input: id of a room
output: state of room
*/
unsigned int RoomManager::getRoomState(int ID)
{
	lck2.lock();
	unsigned int retVal = m_rooms->find(ID)->second.getRoomData().isActive;
	lck2.unlock();
	return retVal;
}

std::vector<RoomData> RoomManager::getRooms()
{
	std::vector<RoomData> rdVec;
	lck2.lock();
	std::map<unsigned int, Room>::iterator it = m_rooms->begin();
	for (it; it != m_rooms->end(); it++)
	{
		if (it->second.getRoomData().isActive == 1) 
		{
			rdVec.push_back(it->second.getRoomData());
		}
	}
	lck2.unlock();
	return rdVec;
}

Room& RoomManager::getRoom(int id)
{
	lck2.lock();
	if (id >= m_rooms->size()) throw std::exception("there is no room with that id");
	lck2.unlock();
	return m_rooms->find(id)->second;
}

