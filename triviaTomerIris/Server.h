#pragma once
#pragma comment (lib, "ws2_32.lib")

#include "WSAInitializer.h"
#include "Server.h"
#include <iostream>
#include <exception>
#include <WinSock2.h>
#include <Windows.h>
#include <thread>
#include "Communicator.h"
#include <string>

#define EXIT "Exit"

class Server
{
public:
	Server();
	~Server();
	void run();
private:
	Communicator m_communictaor;
};

