#include "StatisticsManager.h"

StatisticsManager::StatisticsManager(IDatabase* pDB):m_database{pDB}
{
}

StatisticsManager::StatisticsManager()
{
}

/*
getter to the statistics of a user
*/
std::vector<std::string> StatisticsManager::getUserStatistics(std::string username)
{
	std::vector<std::string> statistics;
	statistics.push_back(std::to_string(this->m_database->getPlayerAverageAnswerTime(username)));
	statistics.push_back(std::to_string(this->m_database->getNumOfCorrectAnswers(username)));
	statistics.push_back(std::to_string(this->m_database->getNumOfTotalAnswers(username)));
	statistics.push_back(std::to_string(this->m_database->getNumOfPlayerGames(username)));
	return statistics;
}

std::vector<std::string> StatisticsManager::getHighScore()
{
	float high_scores[3] = { -1, -1, -1};
	std::string high_scores_users[3];
	float  score = 0;	
	std::vector<std::string> statistics;
	std::vector<std::string> users = this->m_database->getAllUsers();

	for (int i = 0; i < users.size(); i++)
	{
		statistics = getUserStatistics(users[i]);
		if (std::stoi(statistics[2]) != 0)
		{
			score = (std::stof(statistics[1]) / std::stof(statistics[2])) * std::stof(statistics[0]);
		}
		else score = 0;
		std::cout << users[i] + ": " + std::to_string(score) << std::endl;
		if (score >= high_scores[0])
		{
			high_scores[2] = high_scores[1];
			high_scores[1] = high_scores[0];
			high_scores[0] = score;
			high_scores_users[2] = high_scores_users[1];
			high_scores_users[1] = high_scores_users[0];
			high_scores_users[0] = users[i];
		}
		else if (score >= high_scores[1])
		{
			high_scores[2] = high_scores[1];
			high_scores[1] = score;
			high_scores_users[2] = high_scores_users[1];
			high_scores_users[1] = users[i];

		}
		else if(score >= high_scores[2])
		{
			high_scores[2] = score;
			high_scores_users[2] = users[i];
		}
	}
	return std::vector<std::string> (std::begin(high_scores_users), std::end(high_scores_users));
}

