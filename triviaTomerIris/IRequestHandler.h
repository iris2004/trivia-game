#pragma once
#include <ctime>
#include <vector>
#include <string>
#include "JsonResponsePacketSerializer.h"

class IRequestHandler;
struct RequestResult;
struct RequestInfo;
struct RequestInfo
{
	char id;
	std::string recivalTime;
	BufferChar buffer;

};
struct RequestResult
{
	BufferChar response;
	IRequestHandler* newHandler;
};

//class responsible to handle with the rwquests of a client
class IRequestHandler
{

public:
    virtual	bool isRequestRelevant(RequestInfo reqIn) = 0;
	virtual RequestResult handleRequest(RequestInfo reqIn) = 0;
};

