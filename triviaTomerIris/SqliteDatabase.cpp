#include "SqliteDatabase.h"

std::string result;
std::vector <Question> questions;
std::vector<std::string> strings;
std::mutex kys1;
std::unique_lock<std::mutex> lck(kys1,std::defer_lock);

int SqliteDatabase::callbackResult(void* data, int argc, char** argv, char** azColname)
{
	result = argv[0];
	return 0;
}

int SqliteDatabase::callbackQuestions(void* data, int argc, char** argv, char** azColname)
{
	Question question;
	std::vector<std::string> answers;
	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColname[i]) == "QUESTION")
		{
			question.setQuestion(argv[i]);
		}
		else if (std::string(azColname[i]) == "RIGHT_OPTION")
		{
			question.setCorrectAnswer(std::stoi(argv[i]));
		}
		else if (std::string(azColname[i]) != "ID")
		{
			answers.push_back(argv[i]);
		}
	}
	question.setAnswers(answers);
	questions.push_back(question);
	return 0;
}

int SqliteDatabase::callbackStrings(void* data, int argc, char** argv, char** azColname)
{
	for (int i = 0; i < argc; i++)
	{
		strings.push_back(argv[i]);
	}
	return 0;
}

/// <summary>
/// opens the dataBase
/// </summary>
SqliteDatabase::SqliteDatabase()
{
	try {
		
		int res = sqlite3_open("TriviaDB.sqlite", &_db);
		
	}
	catch(...){
		
		throw(std::exception("Failed to open DB"));
	}
}
/// <summary>
/// d'tor
/// </summary>
SqliteDatabase::~SqliteDatabase()
{
}
/// <summary>
/// checks if user exists
/// </summary>
/// <param name="username">the name of the user exist</param>
/// <returns>false/true</returns>
bool SqliteDatabase::doesUserExist(std::string username)
{
	char* errMessage = nullptr;
	std::string sqlMsg = "SELECT * FROM USERS WHERE USERNAME = '" + username + "';";
	result.clear();
	try {
		lck.lock();
		int res = sqlite3_exec(this->_db, sqlMsg.c_str(), callbackResult, nullptr, &errMessage);
		lck.unlock();
	}
	catch (...){ 
		throw(std::exception("Failed to check if user exist"));
	};
	if (result.empty()) return false;
	return true;
}
/// <summary>
/// checks if the password match
/// </summary>
/// <param name="username">the name of the user</param>
/// <param name="password">password of the user</param>
/// <returns></returns>
bool SqliteDatabase::doesPasswordMatch(std::string username, std::string password)
{
	char* errMessage = nullptr;
	std::string sqlMsg = "SELECT PASSWORD FROM USERS WHERE USERNAME = '" + username + "';";
	result.clear();
	try {
		lck.lock();
		int res = sqlite3_exec(this->_db, sqlMsg.c_str(), callbackResult, nullptr, &errMessage);
		lck.unlock();
	}
	catch (...) {
		throw(std::exception("Failed to check if password match"));
	};
	if (result != password) return false;
	return true;
}
/// <summary>
/// adds a new user
/// </summary>
/// <param name="username">the name of the user</param>
/// <param name="password"> the password of the user</param>
/// <param name="email">the email of the user</param>
void SqliteDatabase::addNewUser(std::string username, std::string password, std::string email)
{
	char* errMessage = nullptr;
	std::string sqlMsg;
	int res;
	
	try {
		sqlMsg = "INSERT INTO USERS (USERNAME, PASSWORD, EMAIL) VALUES ('" + username + "', '" + password + "', '" + email + "');";
		lck.lock();
		res = sqlite3_exec(this->_db, sqlMsg.c_str(), nullptr, nullptr, &errMessage);
		lck.unlock();
		std::cout << res;

		result.clear();
		sqlMsg = "SELECT ID FROM USERS WHERE USERNAME = '" + username + "';";
		std::unique_lock<std::mutex> lck(kys1);
		res = sqlite3_exec(this->_db, sqlMsg.c_str(), callbackResult, nullptr, &errMessage);
		lck.unlock();
		sqlMsg = "INSERT INTO STATISTICS (USER_ID, AVERAGE_ANSWER_TIME, NUM_OF_CORRECT_ANSWERS, NUM_OF_TOTAL_ANSWERS, NUM_OF_GAMES) VALUES (" + result + ", 0, 0, 0, 0);";
		lck.lock();
		res = sqlite3_exec(this->_db, sqlMsg.c_str(), nullptr, nullptr, &errMessage);
		lck.unlock();
	}
	catch (...) { 
		throw(std::exception("failed to add user"));
	};


}
/// <summary>
/// 
/// </summary>
/// <param name="numOfQuestion"></param>
/// <returns></returns>
std::vector<Question> SqliteDatabase::getQuestions(int numOfQuestion)
{
	char* errMessage = nullptr;
	questions.clear();
	std::string sqlMsg = "SELECT * FROM QUESTIONS LIMIT " + std::to_string(numOfQuestion) + ";";
	try {
		lck.lock();
		int res = sqlite3_exec(this->_db, sqlMsg.c_str(), callbackQuestions, nullptr, &errMessage);
		lck.unlock();
	}
	catch (...) {
		throw(std::exception("failed to get questions")); 
	}
	return questions;
}
/// <summary>
/// 
/// </summary>
/// <param name="username"></param>
/// <returns></returns>
float SqliteDatabase::getPlayerAverageAnswerTime(std::string username)
{
	char* errMessage = nullptr;
	result.clear();
	std::string sqlMsg = "SELECT AVERAGE_ANSWER_TIME FROM STATISTICS WHERE USER_ID = (SELECT ID FROM USERS WHERE USERNAME = '" + username + "');";
	try {
		lck.lock();
		int res = sqlite3_exec(this->_db, sqlMsg.c_str(), callbackResult, nullptr, &errMessage);
		lck.unlock();
	}
	catch (...)
	{
		throw(std::exception("failed to get average answer time"));
	}
	std::cout << "result: " << result << std::endl;
	return std::stof(result);
}
/// <summary>
/// 
/// </summary>
/// <param name="username"></param>
/// <returns></returns>
int SqliteDatabase::getNumOfCorrectAnswers(std::string username)
{
	char* errMessage = nullptr;
	result.clear();
	std::string sqlMsg = "SELECT NUM_OF_CORRECT_ANSWERS FROM STATISTICS WHERE USER_ID = (SELECT ID FROM USERS WHERE USERNAME = '" + username + "');";
	try {
		lck.lock();
		int res = sqlite3_exec(this->_db, sqlMsg.c_str(), callbackResult, nullptr, &errMessage);
		lck.unlock();
	}
	catch (...) {
		throw(std::exception("failed to get num of correct answers"));
	}
	return std::stoi(result);
}
/// <summary>
/// 
/// </summary>
/// <param name="username"></param>
/// <returns></returns>
int SqliteDatabase::getNumOfTotalAnswers(std::string username)
{
	char* errMessage = nullptr;
	result.clear();
	std::string sqlMsg = "SELECT NUM_OF_TOTAL_ANSWERS FROM STATISTICS WHERE USER_ID = (SELECT ID FROM USERS WHERE USERNAME = '" + username + "');";
	try { 
		lck.lock();
		int res = sqlite3_exec(this->_db, sqlMsg.c_str(), callbackResult, nullptr, &errMessage); 
		lck.unlock();
	}
	catch(...) {
		throw(std::exception("failed to get num of total answers"));
	}
	return std::stoi(result);
}
/// <summary>
/// 
/// </summary>
/// <param name="username"></param>
/// <returns></returns>
int SqliteDatabase::getNumOfPlayerGames(std::string username)
{
	char* errMessage = nullptr;
	result.clear();
	std::string sqlMsg = "SELECT NUM_OF_GAMES FROM STATISTICS WHERE USER_ID = (SELECT ID FROM USERS WHERE USERNAME = '" + username + "');";
	try { 
		lck.lock();
		int res = sqlite3_exec(this->_db, sqlMsg.c_str(), callbackResult, nullptr, &errMessage);
		lck.unlock();
	}
	catch (...) 
	{
		throw(std::exception("failed to get num of games"));
	}
	return std::stoi(result);
}

void SqliteDatabase::changeStatistics(float newAverageAnswerTime, int newNumOfCorrectAnswers, int newNumOfTotalAnswers, std::string user)
{
	char* errMessage = nullptr; 
	int numOfGames = this->getNumOfPlayerGames(user);

	if(numOfGames != 0) newAverageAnswerTime = ((this->getPlayerAverageAnswerTime(user) * (numOfGames - 1)) + newAverageAnswerTime) / numOfGames;
	newNumOfCorrectAnswers += this->getNumOfCorrectAnswers(user);
	newNumOfTotalAnswers += this->getNumOfTotalAnswers(user);
	
	std::string sqlMsg = "UPDATE STATISTICS SET AVERAGE_ANSWER_TIME = " + std::to_string(newAverageAnswerTime) + ", NUM_OF_CORRECT_ANSWERS = " + std::to_string(newNumOfCorrectAnswers) + ", NUM_OF_TOTAL_ANSWERS = " + std::to_string(newNumOfTotalAnswers) + ", NUM_OF_GAMES = " + std::to_string(numOfGames + 1)+ "  WHERE USER_ID = (SELECT ID FROM USERS WHERE USERNAME = '" + user + "');";
	try {
		lck.lock();
		int res = sqlite3_exec(this->_db, sqlMsg.c_str(), nullptr, nullptr, &errMessage);
		lck.unlock();
	}
	catch (...) {
		throw(std::exception("failed to change statistics"));
	}
}

/*
method get all users from the data base
input: none
output: vector of the usernames
*/
std::vector<std::string> SqliteDatabase::getAllUsers()
{
	char* errMessage = nullptr;
	strings.clear();
	std::string sqlMsg = "SELECT USERNAME FROM USERS;";
	try {
		lck.lock();
		int res = sqlite3_exec(this->_db, sqlMsg.c_str(), callbackStrings, nullptr, &errMessage); 
		lck.unlock();
	}
	catch (...) { 
		throw(std::exception("failed to get all uaers"));
	}
	return strings;
}

