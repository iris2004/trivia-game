#include "Game.h"
std::mutex kys4;
std::unique_lock<std::mutex> lck4(kys4, std::defer_lock);

/*
C'tor to the class game
input: players int the room, id of the room
output: nonr
*/
Game::Game(std::vector<Question> questions, std::map<std::string, GameData> players, int id)
{
	this->m_questions = questions;
	this->m_players = players;
	this->m_id = id;
	this->m_changeCounter = 0;
	this->answerTime = 0;
	numOfQuestions = questions.size();

	//inituiate all the vars in the game data to 0
	std::map<std::string, GameData>::iterator i;
	for (i = m_players.begin(); i != m_players.end(); i++)
	{
		i->second.correctAnswerCount = 0;
		i->second.wrongAnswerCount = 0;
	}
}

/*
method replace the current question of a user in a new one
input: username
output: none
*/
void Game::getQuestionForUser(std::string user)
{
	if (m_questions.size() == 0) throw std::exception("there are no more questions");
	
	m_players.find(user)->second.currentQuestion = m_questions[0];
	m_changeCounter += 1;
	if (m_changeCounter == m_players.size())
	{
		//if all the users got the new question it will remove it
		m_questions.erase(m_questions.begin());
		m_changeCounter = 0;
	}
}

/*
method submit the answer of a user
input: username, the answer that is submited, the time until the user answeres
output: none
*/
void Game::submitAnswer(std::string user, int answerId, float timeout)
{
	this->answerTime += timeout;
	if (m_players.find(user)->second.currentQuestion.getCorrentAnswer() == answerId + 1)
	{
		m_players.find(user)->second.correctAnswerCount += 1;
	}
	else
	{
		m_players.find(user)->second.wrongAnswerCount += 1;
	}
	if (m_questions.size() == 0) m_players.find(user)->second.averageAnswerTime = answerTime / numOfQuestions; //calcultate the new average time
}

/*
method remove a plater from the game
input: player to remove
output: none
*/
void Game::removePlayer(std::string user)
{
	m_players.erase(user);
}

/*
getter to the id fiels
input: none
output: the id
*/
int Game::getId()
{
	return this->m_id;
}

/*
getter to the players in the game
input: none
ouput: the players in the game
*/
std::map<std::string, GameData> Game::getPlayers()
{
	return m_players;
}

