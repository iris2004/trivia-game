#include "JsonResponsePacketSerializer.h"
/// <summary>
/// formats the msg to json
/// </summary>
/// <param name="strLen">lenght of the msg</param>
/// <param name="buffer">the msg itself in json format</param>
/// <param name="msg">the the contents of msg</param>
/// <returns>the respons</returns>
std::string formatMsg(std::vector<std::string> players)
{
    std::string playersStr = "";
    for (int i = 0; i < players.size(); i++)
    {
        
        playersStr += players[i];
        if(i != players.size()-1)
            playersStr += "#";
    }
    return playersStr;
}
BufferChar formatMsg(String strLen,BufferChar buffer,String msg)
{
    for (int i = 0; i < 4 - strLen.length(); i++)
    {
        buffer.push_back('0');
    }
    for (int i = 0; i < strLen.length(); i++)
    {
        buffer.push_back(strLen[i]);
    }
    for (int i = 0; i < msg.length(); i++)
    {
        buffer.push_back(msg[i]);
    }
    return buffer;
}

String Format(RoomData rd)
{
    
    String jrd = "";//not robert downey jr
    jrd+= "id=" + std::to_string(rd.id) + "#";
    jrd += "isActive=" + std::to_string(rd.isActive) + "#";
    jrd += "maxPlayer=" +std::to_string(rd.maxPlayer) + "#";
    jrd += "name=" + rd.name + "#";
    jrd += "numOfQuestionsInGame=" + std::to_string(rd.numOfQuestionInGame) + "#";
    jrd += "timePerQuestion=" +std::to_string(rd.timePerQuestion);
    return jrd;
}
String Format(GetPlayersInRoomResponse res)
{
    String usersNames = "{\"users\":\"";
    for (int i = 0; i <  res.players.size(); i++)
    {
        if (i == 0)
        {
            usersNames += res.players[i];
        }
        else
        {
            usersNames += "," + res.players[i];
        }
        
    }
    usersNames += "\"}";
    return usersNames;
}
String Format(GetHighScoreResponse res)
{
    String msg = "{\"status\":" + std::to_string(res.status)+",\"statistics\":\"";
    for(int i = 0;i < res.statistics.size();i++)
    {
        if (i == 0)
        {
            msg += res.statistics[i];
        }
        else msg += "," + res.statistics[i];
    }
    msg += "\"}";
    return msg;
}

String Format(GetPersonalStatsResponse res)
{
    String msg = "{";
    msg += "\"avgAnswerTime\":\"" + res.statistics[0] + "\",";
    msg += "\"correctAnswers\":\"" + res.statistics[1] + "\",";
    msg += "\"totalAnswers\":\"" + res.statistics[2] + "\",";
    msg += "\"numOfGames\":\"" + res.statistics[3] + "\"}";

    return msg;
}
std::string Format(std::vector<PlayerResultResponse> pr)
{
    std::string palyersInfo = "\"";
    std::vector<PlayerResultResponse>::iterator it = pr.begin();
    bool flag = true;
    for (it; it != pr.end(); it++)
    {
        if (flag)
        {
            palyersInfo += "userName:" + it->userName + ",correctAnswerCount:" +
                std::to_string(it->correctAnswerCount) + ",wrongAnswerCount:" +
                std::to_string(it->wrongtAnswerCount) +
                ",AverageAnswerTime:" + std::to_string(it->AverageAnswerTime);
            flag = false;
        }
        else {
            palyersInfo += "#userName:" + it->userName + ",correctAnswerCount:" +
                std::to_string(it->correctAnswerCount) + ",wrongAnswerCount:" +
                std::to_string(it->wrongtAnswerCount) +
                ",AverageAnswerTime:" + std::to_string(it->AverageAnswerTime);
        }
    }
    palyersInfo += "\"";
    return palyersInfo;
}
std::string Format(std::map<unsigned int,std::string> pr)
{
    std::map<unsigned int, std::string>::iterator it = pr.begin();
    bool flag = false;
    std::string msg = "{";
    for (it; it != pr.end(); it++)
    {
        if (flag)
        {
            msg += ",";
        }
        msg += "\"" + std::to_string(it->first) + "\":\"" + it->second + "\"";
        flag = true;
    }
    msg += "}";
    return msg;
}
/// <summary>
/// this function converts the struct login response to a json massege
/// </summary>
/// <param name="res">the login response struct</param>
/// <returns>a char vector with the msg</returns>
BufferChar JsonResponsePacketSerializer::serializeResponse(LoginResponse res)
{
    //msgCode is 1
    String msg = "{\"status\":" + std::to_string(res.status) + "}";
    int len = msg.length();
    String strLen = std::to_string(len);
    BufferChar buffer;
    buffer.push_back('1');
    
    return formatMsg(strLen,buffer,msg);
}
/// <summary>
/// this function converts the struct Signupresponse to a json massege
/// </summary>
/// <param name="res">the Signupresponse struct</param>
/// <returns>a char vector with the msg</returns>
BufferChar JsonResponsePacketSerializer::serializeResponse(SignupResponse res)
{
    
    //msgCode is 2
    String msg = "{\"status\":\"" + std::to_string(res.status) + "\"}";
    int len = msg.length();
    String strLen = std::to_string(len);
    BufferChar buffer;
    buffer.push_back('2');
    return formatMsg(strLen, buffer, msg);
}
/// <summary>
/// this function converts the struct login response to a json massege
/// </summary>
/// <param name="res">the ErrorrResponse struct</param>
/// <returns>a char vector with the msg</returns>
BufferChar JsonResponsePacketSerializer::serializeResponse(ErrorResponse res)
{
    //msgCode is 3
    String msg = "{\"status\":\"" + res.message + "\"}";
    int len = msg.length();
    String strLen = std::to_string(len);
    BufferChar buffer;
    buffer.push_back('3');
    return formatMsg(strLen, buffer, msg);
}
/// <summary>
/// respons to the join room request
/// </summary>
/// <param name="res">the response struct</param>
/// <returns>a vector of chars with the msg</returns>
BufferChar JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse res)
{
    String msg = "{\"status\":" + std::to_string(res.status) + "}";
    int len = msg.length();
    String strLen = std::to_string(len);
    BufferChar buffer;
    buffer.push_back('4');
    return formatMsg(strLen, buffer, msg);
}
/// <summary>
/// response to creat room request 
/// </summary>
/// <param name="res"> the response struc</param>
/// <returns>a vector of chars with the msg</returns>
BufferChar JsonResponsePacketSerializer::serializeResponse(CreatRoomResponse res)
{
    
    String msg = "{\"status\":" + std::to_string(res.status) + "}";
    int len = msg.length();
    String strLen = std::to_string(len);
    BufferChar buffer;
    buffer.push_back('5');
    return formatMsg(strLen, buffer, msg);
}
/// <summary>
/// respons to the logout request 
/// </summary>
/// <param name="res">the response struct</param>
/// <returns>a vector of chars with the response </returns>
BufferChar JsonResponsePacketSerializer::serializeResponse(LogoutResponse res)
{
    String msg = "{\"status\":" + std::to_string(res.status) + "}";
    int len = msg.length();
    String strLen = std::to_string(len);
    BufferChar buffer;
    buffer.push_back('6');
    return formatMsg(strLen, buffer, msg);
}
/// <summary>
/// get the Rooms currently availble
/// </summary>
/// <param name="res">a struct contains the rooms</param>
/// <returns>a vector with response</returns>
BufferChar JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse res)
{
    String jsonF = "";
    for (int i = 0; i < res.rooms.size(); i++)
    {
        if (i == 0)
        {
            jsonF += Format(res.rooms[i]);
        }
        else jsonF += "," + Format(res.rooms[i]);
    }
    //jsonF += "";
    int len = jsonF.length();
    String strLen = std::to_string(len);
    BufferChar buffer;
    buffer.push_back('7');

    return formatMsg(strLen, buffer, jsonF);
}
/// <summary>
/// get the player in a room
/// </summary>
/// <param name="res">contains the user in a certain</param>
/// <returns>a vector with response</returns>
BufferChar JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse res)
{
    String msg = Format(res);
    int len = msg.length();
    String strLen = std::to_string(len);
    BufferChar buffer;
    buffer.push_back('8');
    return formatMsg(strLen, buffer, msg);
}
/// <summary>
/// gets the 5 highest scoring user
/// </summary>
/// <param name="res">conatins the stats of the gets the 5 highest scoring user</param>
/// <returns></returns>
BufferChar JsonResponsePacketSerializer::serializeResponse(GetHighScoreResponse res)
{
    String msg = Format(res);
    int len = msg.length();
    String strLen = std::to_string(len);
    BufferChar buffer;
    buffer.push_back('9');
    return formatMsg(strLen, buffer, msg);
}
/// <summary>
/// gets the the personal stats of a user
/// </summary>
/// <param name="res">the struct conatains the user stats</param>
/// <returns></returns>
BufferChar JsonResponsePacketSerializer::serializeResponse(GetPersonalStatsResponse res)
{
    String msg = Format(res);
    int len = msg.length();
    String strLen = std::to_string(len);
    BufferChar buffer;
    buffer.push_back('A');
    return formatMsg(strLen, buffer, msg);
}

/// <summary>
/// admin close the room 
/// </summary>
/// <param name="res"></param>
/// <returns></returns>
BufferChar JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse res)
{
    std::string msg = "{\"status\":\"" + std::to_string(res.status) + "\"}";
    std::string strLen = std::to_string(msg.length());
    BufferChar buffer;
    buffer.push_back('B');
    return formatMsg(strLen, buffer, msg);
}

/// <summary>
/// start a new game in a room
/// </summary>
/// <param name="res"></param>
/// <returns></returns>
BufferChar JsonResponsePacketSerializer::serializeResponse(StartGameResponse res)
{
    std::string msg = "{\"status\":\"" + std::to_string(res.status) + "\"}";
    std::string strLen = std::to_string(msg.length());
    BufferChar buffer;
    buffer.push_back('C');
    return formatMsg(strLen, buffer, msg);
}

/// <summary>
/// get the state of a room
/// </summary>
/// <param name="res">struct contain the details of a room</param>
/// <returns></returns>
BufferChar JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse res)
{
    std::string msg = "{\"status\":\"" + std::to_string(res.status) + "\"";
    msg += ",\"hasGameBegun\":\"" + std::to_string(res.hasGameBegun) + "\"";
    msg += ",\"Players\":\"" + formatMsg(res.players) + "\"";
    msg += ",\"AnswerCount\":" + std::to_string(res.questionCount);
    msg += ",\"AnswerTimeOut\":" + std::to_string(res.answerTimeOut) + "}";
    std::string strLen = std::to_string(msg.length());
    BufferChar buffer;
    buffer.push_back('D');
    return formatMsg(strLen, buffer, msg);
}

/// <summary>
/// remove a player from a room
/// </summary>
/// <param name="res"></param>
/// <returns></returns>
BufferChar JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse res)
{
    std::string msg = "{\"status\":\"" + std::to_string(res.status) + "\"}";
    std::string strLen = std::to_string(msg.length());
    BufferChar buffer;
    buffer.push_back('E');
    return formatMsg(strLen, buffer, msg);
}

/// <summary>
/// get the results of the game
/// </summary>
/// <param name="res">struct contain the results</param>
/// <returns></returns>
BufferChar JsonResponsePacketSerializer::serializeResponse(GetGameResultResponse res)
{
    std::string msg = "{\"status\":\"" + std::to_string(res.status) + "\",\"results\":" + Format(res.results) + "}";
    std::string strLen = std::to_string(msg.length());
    BufferChar buffer;
    buffer.push_back('F');
    return formatMsg(strLen, buffer,msg);
}

/// <summary>
/// user submit an answer to a question
/// </summary>
/// <param name="res">struct contain the setails of the submit</param>
/// <returns></returns>
BufferChar JsonResponsePacketSerializer::serializeResponse(SubmitAnswersResponse res)
{
    std::string msg = "{\"status\":\"" + std::to_string(res.status) + "\",\"correctAnswerId\":" + std::to_string(res.correctAnswerId) + "}";
    std::string strLen = std::to_string(msg.length());
    BufferChar buffer;
    buffer.push_back('G');
    return formatMsg(strLen, buffer, msg);
}

/// <summary>
/// get a new question
/// </summary>
/// <param name="res">struct contain the details of the question</param>
/// <returns></returns>
BufferChar JsonResponsePacketSerializer::serializeResponse(GetQuestionResponse res)
{
    std::string msg = "{\"status\":\"" + std::to_string(res.status) + "\",\"question\":\"" +res.question + "\",\"answers\":"+Format(res.answers) +"}";
    std::string strLen = std::to_string(msg.length());
    BufferChar buffer;
    buffer.push_back('H');
    return formatMsg(strLen, buffer, msg);
}

/// <summary>
/// remove a user from a game
/// </summary>
/// <param name="res"></param>
/// <returns></returns>
BufferChar JsonResponsePacketSerializer::serializeResponse(LeaveGameResponse res)
{
    std::string msg = "{\"status\":\"" + std::to_string(res.status) + "\"}";
    std::string strLen = std::to_string(msg.length());
    BufferChar buffer;
    buffer.push_back('I');
    return formatMsg(strLen, buffer, msg);
}
