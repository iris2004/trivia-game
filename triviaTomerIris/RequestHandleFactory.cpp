#include "RequestHandleFactory.h"
#include "MenuRequestHandler.h"
/// <summary>
/// c'tor
/// </summary>
RequestHandleFactory::RequestHandleFactory(): m_roomManager{*(new RoomManager())}
{
    //SqliteDatabase d;
    //this->m_database = &d;
    //this->m_LoginManager.setDataBase(&d);
    m_statisticsManager = StatisticsManager(m_loginManager.getDataBase());
}

/// <summary>
/// creats a login request handler
/// </summary>
/// <returns>createLoginRequestHandler</returns>
LoginRequestHandler* RequestHandleFactory::createLoginRequestHandler()
{
    // TODO: insert return statement here
    return new LoginRequestHandler (&this->m_loginManager, this);
}

/// <summary>
/// retrun the login manager
/// </summary>
/// <returns>LoginManager&</returns>
LoginManager& RequestHandleFactory::getLoginManager()
{
    return m_loginManager;
}

/*
getter to the statisticsMnager field
input: none
output: StatisticsManager
*/
StatisticsManager& RequestHandleFactory::getStatisticsManager()
{
    return m_statisticsManager;
}

/*
getter to the roomManager field
input: none
output: roomManager
*/
RoomManager& RequestHandleFactory::getRoomManager()
{
    RoomManager& rm = m_roomManager;
    return rm;
}

/*
method create a new handler
input: room
output: RoomAdminRequestHandler
*/
RoomAdminRequestHandler* RequestHandleFactory::createRoomAdminRequestHandler(Room& room)
{
    return new RoomAdminRequestHandler(this, room);
}

/*
method create a new handler
input: room
output: RoomMemberRequestHandler
*/
RoomMemberRequestHandler* RequestHandleFactory::createRoomMemberRequestHandler(Room& room)
{
    return new RoomMemberRequestHandler(this, room);
}

/*
method create a new handler
input: room
output: GameRequestHandler
*/
GameRequestHandler* RequestHandleFactory::createGameRequestHandler(Room& room)
{
    return new GameRequestHandler(this, room);
}

/*
getter to the gameManager
input: none
output: GameManager
*/
GameManager& RequestHandleFactory::getGameManager()
{
    return this->m_gameManager;;
}

/*
method create a new handler
input: none
output: MenuRequestHandler
*/
MenuRequestHandler* RequestHandleFactory::createMenueHandler()
{
    return new MenuRequestHandler(this);
}

/*
setter to the user
input: userName
output: none
*/
void RequestHandleFactory::setUser(std::string user)
{
    m_user = user;
}

/*
getter to the user field
input: none
output: none
*/
std::string RequestHandleFactory::getUser()
{
    return m_user;
}