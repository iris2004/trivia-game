#include "JsonRequestPacketDeserializer.h"
/// <summary>
/// takes the client request and takes the data from the 
/// request and inputs it into a struct for later use
/// </summary>
/// <param name="buffer">the client request a vector of unsigned chars</param>
/// <returns>return a struct with the request data</returns>
LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(BufferChar buffer)
{
    LoginRequest res;

    json j = json::parse(buffer.begin(), buffer.end());

    j.at("username").get_to(res.username);
    j.at("password").get_to(res.password);
    return res;
}
/// <summary>
/// takes the client request and takes the data from the 
/// request and inputs it into a struct for later use
/// </summary>
/// <param name="buffer">the client request a vector of unsigned chars</param>
/// <returns>return a struct with the request data</returns>
SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(BufferChar buffer)
{
    SignupRequest res;
    json j = json::parse(buffer.begin(), buffer.end());

    res.username = j["username"];
    j.at("password").get_to(res.password);
    j.at("email").get_to(res.email);
    return res;
}

/// <summary>
/// deserialize a request to get players
/// </summary>
/// <param name="buffer">the client request a vector of unsigned chars</param>
/// <returns>return a struct with the request data</returns>
GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersRequest(BufferChar buffer)
{
    GetPlayersInRoomRequest res;
    json j = json::parse(buffer.begin(), buffer.end());
    j.at("roomId").get_to(res.roomId);
    return res;
}

/// <summary>
/// deserialize a request join room
/// </summary>
/// <param name="buffer">the client request a vector of unsigned chars</param>
/// <returns>return a struct with the request data</returns>
JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(BufferChar buffer)
{
    JoinRoomRequest res;
    json j = json::parse(buffer.begin(), buffer.end());
    j.at("roomId").get_to(res.roomId);
    return res;
}

/// <summary>
/// deserialize a create a room request
/// </summary>
/// <param name="buffer">the client request a vector of unsigned chars</param>
/// <returns>return a struct with the request data</returns>
CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(BufferChar buffer)
{
    CreateRoomRequest res;
    json j = json::parse(buffer.begin(), buffer.end());
    j.at("roomName").get_to(res.roomName);
    j.at("questionCount").get_to(res.questionCount);
    j.at("maxUsers").get_to(res.maxUsers);
    j.at("answerTimeOut").get_to(res.answerTimeOut);
    return res;
}

/// <summary>
/// deserialize a submit answer request
/// </summary>
/// <param name="buffer"></param>
/// <returns></returns>
SubmitAnswerRequest JsonRequestPacketDeserializer::deserializerSubmitAnswerRequest(BufferChar buffer)
{
    SubmitAnswerRequest res;
    json j = json::parse(buffer.begin(), buffer.end());
    j.at("answerid").get_to(res.answerid);
    j.at("answerTime").get_to(res.answerTime);
    return res;
}




