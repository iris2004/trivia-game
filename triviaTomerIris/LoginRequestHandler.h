#pragma once
#define _CRT_SECURE_NO_DEPRECATE
#include "IRequestHandler.h"
#include "LoginManager.h"
#include "RequestHandleFactory.h"
#include "JsonRequestPacketDeserializer.h"
#include <iostream>
#include "MenuRequestHandler.h"

#define LOGIN_CODE '1'
#define SIGNUP_CODE '2'

struct RequestInfo;
struct RequestResult;
class RequestHandleFactory;

//class responsible to handle all the login and sign op requests
class LoginRequestHandler :
    public IRequestHandler
{
private:
    LoginManager& m_loginManager;
    RequestHandleFactory& m_handlerFactory;
    RequestResult login(RequestInfo reqIn);
    RequestResult signup(RequestInfo reqIn);
    
public:
    LoginRequestHandler(LoginManager* m, RequestHandleFactory* rf);
    bool isRequestRelevant(RequestInfo req);
    RequestResult handleRequest(RequestInfo req);
    LoginManager& getLoginManger();
    RequestHandleFactory& getHandlerFactory();
};

