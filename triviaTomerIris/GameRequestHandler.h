#pragma once

#include "IRequestHandler.h"
#include "RequestHandleFactory.h"
#include "GameManager.h"

#define GET_GAME_RESULT_REQUEST_CODE 'F'
#define SUBMIT_ANSWER_REQUEST_CODE 'G'
#define GET_QUESTION_REQUEST_CODE 'H'
#define LEAVE_GAME_REQUEST_CODE 'I'
#define NUM_OF_POSSIBLE_ANSWERS 4

class RequestHandleFactory;

//class responsible for all the game related requests
class GameRequestHandler : public IRequestHandler
{
private:
	Game m_game;
	LoggedUser m_user;
	GameManager& m_gameManager;
	RequestHandleFactory& m_handlerFacroty;

	RequestResult getQuestion(RequestInfo reqIn);
	RequestResult submitAnswer(RequestInfo reqIn);
	RequestResult getGameResults(RequestInfo reqIn);
	RequestResult leaveGame(RequestInfo reqIn);
public:
	GameRequestHandler(RequestHandleFactory* handlerFacroty, Room& room);
	virtual bool isRequestRelevant(RequestInfo reqIn);
	virtual RequestResult handleRequest(RequestInfo reqIn);
};

