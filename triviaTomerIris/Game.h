#pragma once

#include "Question.h"
#include "LoggedUser.h"
#include <map>
#include <mutex>

//the data of every game
struct GameData {
	Question currentQuestion;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	float averageAnswerTime;
};

class Game
{
private:
	int m_id; //the id of each game is identical to the id of the room
	std::vector<Question> m_questions; 
	std::map<std::string, GameData> m_players;
	int m_changeCounter; //how many clients got the new current question
	float answerTime;
	int numOfQuestions;
public:
	Game(std::vector<Question> questions, std::map<std::string, GameData> players, int id);
	void getQuestionForUser(std::string user);
	void submitAnswer(std::string user, int answerId, float timeout);
	void removePlayer(std::string user);
	int getId();
	std::map<std::string, GameData> getPlayers();
};
