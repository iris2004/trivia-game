#pragma once
#include <map>
#include "Room.h"
#include <iostream>
#include <mutex>

//class responsible to namge the room actions
class RoomManager
{
private:
	static std::map<unsigned int, Room>* m_rooms;
public:
	RoomManager();
	void createRoom(LoggedUser user, RoomData& rd);
	void deleteRoom(int ID);
	unsigned int getRoomState(int ID);
	std::vector<RoomData> getRooms();
	Room& getRoom(int id);

};

