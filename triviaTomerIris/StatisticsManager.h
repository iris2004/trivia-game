#pragma once

#include "IDatabase.h"
#include <vector>
#include <string>

class StatisticsManager
{
private:
	IDatabase* m_database;
public:
	StatisticsManager(IDatabase* pDB);
	StatisticsManager();
	std::vector<std::string> getUserStatistics(std::string username);
	std::vector<std::string> getHighScore();
};

