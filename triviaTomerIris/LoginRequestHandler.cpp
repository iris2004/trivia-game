#include "LoginRequestHandler.h"

/// <summary>
/// gets the user request and trys to log him in
/// </summary>
/// <param name="reqIn">the request information</param>
/// <returns>return the result</returns>
RequestResult LoginRequestHandler::login(RequestInfo reqIn)
{
    RequestResult res;
    LoginResponse logRes;
    logRes.status = 1;
    LoginRequest logReq = JsonRequestPacketDeserializer::deserializeLoginRequest(reqIn.buffer);
    this->m_loginManager.login(logReq.username, logReq.password);
    this->m_handlerFactory.setUser(logReq.username);
    BufferChar response = JsonResponsePacketSerializer::serializeResponse(logRes);
    res.response = response;
    res.newHandler = m_handlerFactory.createMenueHandler();
    return res;
}

/// <summary>
/// trys to sign up the user
/// </summary>
/// <param name="reqIn">request information</param>
/// <returns>the request result</returns>
RequestResult LoginRequestHandler::signup(RequestInfo reqIn)
{
    RequestResult res;
    SignupResponse signRes;
    signRes.status = 0; 
    SignupRequest signReq = JsonRequestPacketDeserializer::deserializeSignupRequest(reqIn.buffer);
    this->m_loginManager.signup(signReq.username, signReq.password, signReq.email);
    std::string username = signReq.username;
    this->m_handlerFactory.setUser(username);
    BufferChar response = JsonResponsePacketSerializer::serializeResponse(signRes);
    res.response = response;
    res.newHandler = m_handlerFactory.createMenueHandler();
    return res;
}

/// <summary>
/// c'tor
/// </summary>
/// <param name="m">LoginManager</param>
/// <param name="rf">RequestHandleFactory</param>
LoginRequestHandler::LoginRequestHandler(LoginManager* m,RequestHandleFactory* rf):m_handlerFactory{*rf}, m_loginManager{ *m }
{
}

/// <summary>
/// checks if the request is relevant
/// </summary>
/// <param name="req">the request</param>
/// <returns>true/false</returns>
bool LoginRequestHandler::isRequestRelevant(RequestInfo req)
{
    if (req.id == '1' || req.id == '2') return true;
    return false;
}

/// <summary>
/// handles the request
/// </summary>
/// <param name="req">the request</param>
/// <returns>the result</returns>
RequestResult LoginRequestHandler::handleRequest(RequestInfo req)
{
    RequestResult r;
    try
    {
        if (this->isRequestRelevant(req))
        {
            if (req.id == LOGIN_CODE)
            {
                r = this->login(req);
            }
            else if (req.id == SIGNUP_CODE)
            {
                r = this->signup(req);
            }
        }
        else throw std::exception("request is irelevant");
    }  
    catch(std::exception &s)
    {
        ErrorResponse res;
        res.message = s.what();
        r.response = JsonResponsePacketSerializer::serializeResponse(res);
        r.newHandler = this;
    }
    return r;
}

/// <summary>
/// retrun the login manager
/// </summary>
/// <returns>LoginManager</returns>
LoginManager& LoginRequestHandler::getLoginManger()
{
    return this->m_loginManager;
}

/// <summary>
/// return RequestHandleFactory
/// </summary>
/// <returns>RequestHandleFactory</returns>
RequestHandleFactory& LoginRequestHandler::getHandlerFactory()
{
    return this->m_handlerFactory;
}
