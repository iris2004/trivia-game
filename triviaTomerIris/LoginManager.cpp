#include "LoginManager.h"

std::vector<LoggedUser> LoginManager::m_loggedUsers;
IDatabase* LoginManager::m_database = new SqliteDatabase();
std::mutex kys3;
std::unique_lock<std::mutex> lck1(kys3,std::defer_lock);

/// <summary>
/// c'tor
/// </summary>
LoginManager::LoginManager(){}

/// <summary>
/// adds the user to the data base
/// </summary>
/// <param name="username">the name of the user</param>
/// <param name="password">the password of the user</param>
/// <param name="email">the email of user</param>
void LoginManager::signup(std::string username, std::string password, std::string email)
{
	if (this->m_database->doesUserExist(username))
	{
		throw std::exception("a user already exists");
	}
	else {
		this->m_database->addNewUser(username, password, email);
	}
}

/// <summary>
/// login the user
/// </summary>
/// <param name="username">the name of the user</param>
/// <param name="password">the password of the user</param>
void LoginManager::login(std::string username, std::string password)
{
	lck1.lock();
	for (int i = 0; i < m_loggedUsers.size(); i++)
	{
		if (m_loggedUsers[i].getUsername() == username)
		{
			lck1.unlock();
			throw std::exception("user already logged");
		}
	}
	if (!m_database->doesUserExist(username))
	{
		lck1.unlock();
		throw std::exception("user doesn't exist");
	}

	else if (m_database->doesPasswordMatch(username, password))
	{
		m_loggedUsers.push_back(LoggedUser(username));
	}
	else
	{
		lck1.unlock();
		throw std::exception("password isn't correct");
	}
	lck1.unlock();
}

/// <summary>
/// logs out the user
/// </summary>
/// <param name="username">the name of the user</param>
void LoginManager::logout(std::string username)
{
	bool check = false;
	lck1.lock();
	for (int i = 0; i < this->m_loggedUsers.size(); i++)
	{
		if (this->m_loggedUsers[i].getUsername() == username)
		{
			this->m_loggedUsers.erase(m_loggedUsers.begin() + i);
			check = true;
		}
	}
	lck1.unlock();
	if (!check) throw std::exception("user doesn't exist");
}

/// <summary>
/// getter to the data base
/// </summary>
/// <returns></returns>
IDatabase* LoginManager::getDataBase()
{
	return this->m_database;
}