#pragma once

#include <iostream>
#include <list>
#include <algorithm>
#include <vector>
#include "Question.h"

#define NUM_OF_OPTIONS 4

//class responsible to comunicate with the data base
class IDatabase
{
public:
	virtual bool doesUserExist(std::string username) = 0;
	virtual bool doesPasswordMatch(std::string username, std::string password) = 0;
	virtual void addNewUser(std::string username, std::string password, std::string email) = 0;
	virtual std::vector<std::string> getAllUsers() = 0;
	virtual std::vector<Question> getQuestions(int numOfQuestion) = 0;
	virtual float getPlayerAverageAnswerTime(std::string username) = 0;
	virtual int getNumOfCorrectAnswers(std::string username) = 0;
	virtual int getNumOfTotalAnswers(std::string username) = 0;
	virtual int getNumOfPlayerGames(std::string username) = 0;
	virtual void changeStatistics(float newAverageAnswerTime, int newNumOfCorrectAnswers, int newNumOfTotalAnswers, std::string user) = 0;
};
