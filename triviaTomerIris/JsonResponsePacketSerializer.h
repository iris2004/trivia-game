#pragma once
#include <iostream>
#include"json.hpp"
#include <vector>
#include <string>
#include "Room.h"
#define String std::string 
#define BufferChar std::vector<unsigned char>

struct LoginResponse
{
	unsigned int status;
};
struct SignupResponse
{
	unsigned int status;
};
struct ErrorResponse
{
	String message;
};
struct JoinRoomResponse
{
	unsigned int status;
};
struct CreatRoomResponse
{
	unsigned int status;
};
struct LogoutResponse
{
	unsigned int status;
};
struct GetRoomsResponse
{
	unsigned int status;
	std::vector<RoomData> rooms;
};
struct GetPlayersInRoomResponse
{
	std::vector<String> players;
};
struct GetHighScoreResponse
{
	unsigned int status;
	std::vector<String> statistics;
};
struct GetPersonalStatsResponse
{
	unsigned int status;
	std::vector<String> statistics;
};
struct CloseRoomResponse{
	unsigned int status;
};
struct StartGameResponse {
	unsigned int status;
};
struct GetRoomStateResponse {
	unsigned int status;
	unsigned int hasGameBegun;
	std::vector<std::string> players;
	unsigned int questionCount;
	unsigned int answerTimeOut;
};
struct LeaveRoomResponse
{
	unsigned int status;
};
struct LeaveGameResponse
{
	unsigned int status;
};
struct GetQuestionResponse
{
	unsigned int status;
	std::string question;
	std::map<unsigned int, std::string> answers;
};

struct SubmitAnswersResponse
{
	unsigned int status;
	unsigned int correctAnswerId;

};

struct PlayerResultResponse
{
	std::string userName;
	unsigned int correctAnswerCount;
	unsigned int wrongtAnswerCount;
	float AverageAnswerTime;

};

struct GetGameResultResponse
{
	unsigned int status;
	std::vector<PlayerResultResponse> results;
};

//class responsible to serialize all the messages to send to the clients
class JsonResponsePacketSerializer
{
public:
	static BufferChar serializeResponse(LoginResponse res); //code 1
	static BufferChar serializeResponse(SignupResponse res); //code 2
	static BufferChar serializeResponse(ErrorResponse res); //code 3
	static BufferChar serializeResponse(JoinRoomResponse res); //code 4
	static BufferChar serializeResponse(CreatRoomResponse res); //code 5
	static BufferChar serializeResponse(LogoutResponse res); //code 6
	static BufferChar serializeResponse(GetRoomsResponse res); //code 7
	static BufferChar serializeResponse(GetPlayersInRoomResponse res); //code 8
	static BufferChar serializeResponse(GetHighScoreResponse res); //code 9
	static BufferChar serializeResponse(GetPersonalStatsResponse res);//code A
	static BufferChar serializeResponse(CloseRoomResponse res); //code B
	static BufferChar serializeResponse(StartGameResponse res); //code C
	static BufferChar serializeResponse(GetRoomStateResponse res); //code D
	static BufferChar serializeResponse(LeaveRoomResponse res); //code E
	static BufferChar serializeResponse(GetGameResultResponse res); //code F
	static BufferChar serializeResponse(SubmitAnswersResponse res); //code G
	static BufferChar serializeResponse(GetQuestionResponse res); //code H
	static BufferChar serializeResponse(LeaveGameResponse res); //code I
};


