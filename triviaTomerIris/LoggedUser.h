#pragma once

#include <iostream>

//class that represent a looged username
class LoggedUser
{
private:
	std::string m_username;
public:
	LoggedUser();
	LoggedUser(std::string username);
	~LoggedUser();
	std::string getUsername();
};