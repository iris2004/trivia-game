#pragma once

#include "SqliteDatabase.h"
#include "Room.h"
#include <map>
#include "Game.h"

//class manage the game prosess
class GameManager
{
private:
	static IDatabase* m_database;
	static std::vector<Game> m_games;
public:
	Game createGame(Room room);
	void deleteGame(Game game);
	IDatabase* getDataBase();
};