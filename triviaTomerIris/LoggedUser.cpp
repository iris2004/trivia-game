#include "LoggedUser.h"

LoggedUser::LoggedUser()
{
}

/// <summary>
/// c'tor
/// </summary>
/// <param name="username"></param>
LoggedUser::LoggedUser(std::string username)
{
    this->m_username = username;
}

/// <summary>
/// d'tor
/// </summary>
LoggedUser::~LoggedUser()
{
}

/// <summary>
/// return the name of the user
/// </summary>
/// <returns>return a string that conatains the users name</returns>
std::string LoggedUser::getUsername()
{
    return this->m_username;
}
