#include "PlayerInRoomRequestHandler.h"

/*
method start a new game
input: a room
output: result of the request
*/
RequestResult PlayerInRoomRequestHandler::startGame(Room& room)
{
	RequestResult reqRes;
	StartGameResponse res;
	res.status = 1;
	reqRes.response = JsonResponsePacketSerializer::serializeResponse(res);
	return reqRes;
}

/*
method get the state of a room
input: room
output: result of the request
*/
RequestResult PlayerInRoomRequestHandler::getRoomState(Room& room)
{
	RequestResult reqRes;
	GetRoomStateResponse res;
	res.status = 1;
	res.answerTimeOut = room.getRoomData().timePerQuestion;
	res.hasGameBegun = room.getRoomData().isActive;
	res.players = room.getAllUsers();
	res.questionCount = room.getRoomData().numOfQuestionInGame;
	reqRes.response = JsonResponsePacketSerializer::serializeResponse(res);
	return reqRes;
}