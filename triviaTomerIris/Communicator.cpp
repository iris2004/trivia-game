#include "Communicator.h"
/// <summary>
/// this function binds the socket which the server use 
/// and listens on it
/// </summary>
void Communicator::bindAndListen()
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(SERVER_PORT); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(m_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(m_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << SERVER_PORT << std::endl;

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		// notice that we step out to the global namespace
		// for the resolution of the function accept
		SOCKET client_socket = ::accept(m_serverSocket, NULL, NULL);

		if (client_socket == INVALID_SOCKET)
			throw std::exception(__FUNCTION__);

		std::cout << "Client accepted. Server and client can speak" << std::endl;

		// the function that handle the conversation with the client

		std::thread t(&Communicator::handleNewClient, this, client_socket);
		t.detach();
	}
}
/// <summary>
/// send and recives masseges from a client
/// </summary>
/// <param name="sock">the socket that the client uses to communicate with the server</param>
void Communicator::handleNewClient(SOCKET sock)
{
	RequestInfo reqIn;
	LoginManager m;
	char startMsg[7];
	BufferChar msg(5000);
	int result;
	IRequestHandler* n = new LoginRequestHandler(&m, new RequestHandleFactory());
	try
	{		
			do{

				startMsg[6] = 0;
				result = recv(sock, startMsg, 5, 0);
				if (result != 0)
				{
					std::cout << result << std::endl;
					char msgCode = startMsg[0];
					std::string length = startMsg;

					int len = std::stoi(length.substr(1, 4));
					msg.resize(len);

					result = recv(sock, (char*)msg.data(), msg.size(), 0);
					std::cout << "part 1 reached" << std::endl;
					std::time_t time = std::time(nullptr);
					reqIn.recivalTime = std::ctime(&time);
					reqIn.id = msgCode;
					//std::cout << msg.size() << std::endl;
					reqIn.buffer.clear();
					for (int i = 0; i < msg.size(); i++)
					{
						reqIn.buffer.push_back(msg[i]);
					}
					RequestResult reqRes = n->handleRequest(reqIn);
					//for (int i = 0; i < reqRes.response.size(); i++) std::cout << reqRes.response[i];
					//std::cout << std::endl;
					send(sock, (const char*)reqRes.response.data(), reqRes.response.size(), 0);
					n = reqRes.newHandler;
				}
			} while (result != SOCKET_ERROR);
		closesocket(sock);
	}
	catch (const std::exception& e)
	{
		
		closesocket(sock);
		std::cout << e.what() << std::endl;
	}
}

/// <summary>
/// starts the communication with the client
/// </summary>
void Communicator::startHandleRequests()
{
	bindAndListen();
}

/// <summary>
/// creats the server socket
/// </summary>
Communicator::Communicator()
{
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	m_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (m_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
	//bindAndListen();
}

/// <summary>
/// closes the server socket
/// </summary>
Communicator::~Communicator()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(m_serverSocket);
	}
	catch (...) {}
}