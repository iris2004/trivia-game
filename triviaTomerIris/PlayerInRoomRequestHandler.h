#pragma once

#include "RequestHandleFactory.h"
#include "RoomManager.h"

#define CLOSE_ROOM_REQUEST_CODE 'B'
#define START_GAME_REQUEST_CODE 'C'
#define GET_ROOM_STATE_REQUEST_CODE 'D'
#define LEAVE_ROOM_REQUEST_CODE 'E'
#define ROOM_CLOSED 0
#define ROOM_OPEN 1
#define GAME_BEGUN 2

//class handle the requests of a player in a room
class PlayerInRoomRequestHandler
{
public:
	static RequestResult startGame(Room& room);
	static RequestResult getRoomState(Room& room);
};