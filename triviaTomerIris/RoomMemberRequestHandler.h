#pragma once

#include "PlayerInRoomRequestHandler.h"
#include "JsonResponsePacketSerializer.h"

class PlayerInRoomRequestHandler;
class RequestHandleFactory;

class RoomMemberRequestHandler : public IRequestHandler
{
private:
	Room& m_room;
	LoggedUser m_user;
	RoomManager& m_roomManager;
	RequestHandleFactory& m_handlerFactory;

	RequestResult leaveRoom();

public:
	RoomMemberRequestHandler(RequestHandleFactory* requestHandleFactory, Room& room);
	virtual bool isRequestRelevant(RequestInfo req);
	virtual RequestResult handleRequest(RequestInfo req);
};