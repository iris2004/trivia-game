#include "RoomMemberRequestHandler.h"

/*
method remove a user from the room
input: lnone
output: results of the request
*/
RequestResult RoomMemberRequestHandler::leaveRoom()
{
	RequestResult reqRes;
	LeaveRoomResponse res;
	m_room.removeUser(m_user.getUsername());
	if (m_room.getAllUsers().size() == 0)
	{
		m_roomManager.deleteRoom(m_room.getRoomData().id);
	}
	res.status = 1;
	reqRes.response = JsonResponsePacketSerializer::serializeResponse(res);
	reqRes.newHandler = new MenuRequestHandler(&m_handlerFactory);
	return reqRes;
}

/*
C'tor to the class
*/
RoomMemberRequestHandler::RoomMemberRequestHandler(RequestHandleFactory* requestHandleFactory, Room& room) : IRequestHandler(), 
m_handlerFactory(*requestHandleFactory),
m_roomManager(requestHandleFactory->getRoomManager()),
m_room(room)
{
	m_user = m_handlerFactory.getUser();
}

/*
method check if the request id relevant
input: info of the request
output: true/false
*/
bool RoomMemberRequestHandler::isRequestRelevant(RequestInfo req)
{
	if (req.id == LEAVE_ROOM_REQUEST_CODE || req.id == START_GAME_REQUEST_CODE || req.id == GET_ROOM_STATE_REQUEST_CODE)
	{
		return true;
	}
	return false;
}

/*
method handle the new request from the client
input: info of the request
output: result of the request
*/
RequestResult RoomMemberRequestHandler::handleRequest(RequestInfo req)
{
	RequestResult reqRes;
	try {
		if (!isRequestRelevant(req)) throw std::exception("request is irelevant");
		if (req.id == LEAVE_ROOM_REQUEST_CODE)
		{
			reqRes = leaveRoom();
		}
		else if (req.id == START_GAME_REQUEST_CODE)
		{
			reqRes = PlayerInRoomRequestHandler::startGame(m_room);
			reqRes.newHandler = m_handlerFactory.createGameRequestHandler(m_room);
			m_room.changeRoomState(2);
		}
		else if (req.id == GET_ROOM_STATE_REQUEST_CODE)
		{
			reqRes = PlayerInRoomRequestHandler::getRoomState(m_room);
			reqRes.newHandler = this;
		}
	}
	catch (std::exception& s)
	{
		ErrorResponse res;
		res.message = s.what();
		reqRes.response = JsonResponsePacketSerializer::serializeResponse(res);
		reqRes.newHandler = (IRequestHandler*)this;
		std::cout << s.what() << std::endl;
	}
	return reqRes;
}
