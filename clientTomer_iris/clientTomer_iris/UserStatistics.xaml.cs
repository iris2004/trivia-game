﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace clientTomer_iris
{
    /// <summary>
    /// Interaction logic for UserStatistics.xaml
    /// </summary>
    public partial class UserStatistics : Window
    {
        internal class UserStats
        {
            public string avgAnswerTime;
            public string correctAnswers;
            public string totalAnswers;
            public string numOfGames;
        }

        public UserStatistics()
        {
            string msg = "A0002{}";
            serverConnection.sendMsg(msg);

            int len = Convert.ToInt32(serverConnection.readMsg(5).Substring(1));
            msg = serverConnection.readMsg(len);
            UserStats res = new UserStats();
            res = JsonConvert.DeserializeObject<UserStats>(msg);

            InitializeComponent();

            this.tAvgAnsTime.Text = res.avgAnswerTime;
            this.tCorrectAnswers.Text = res.correctAnswers;
            this.tTotalAnswers.Text = res.totalAnswers;
            this.tnumOfGames.Text = res.numOfGames;         
        }
        private void Quit_click(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string msg = "60002{}";
            serverConnection.sendMsg(msg);
            string format = serverConnection.readMsg(5);
            int len = Convert.ToInt32(format.Substring(1));
            string msgNew = serverConnection.readMsg(len);
            Environment.Exit(0);
        }
        private void GoBack(object sender, RoutedEventArgs e)
        {
            Statistics win = new Statistics();
            win.Show();
            this.Hide();
        }

    }
}
