﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;

static public class txb
{
    static public List<TextBlock> myTextBlk = new List<TextBlock>();
}

namespace clientTomer_iris
{
    /// <summary>
    /// Interaction logic for PlayersInRoom.xaml
    /// </summary

    
    public partial class PlayersInRoom : Window
    {
        //delegate void update_window();
        delegate void update_players_callback();
        private Thread tr;
        private Run r = new Run();


        private uint HEIGHT = 25;
        private uint WIDTH = 100;
        private uint STEP = 2;
        private int currentX = 10;
        private int currentY = 35;
        private bool isAdmin;
        private uint numOfQuestions;
        private uint timeOut;
        
        internal class StartGameResponse
        {
            public string status;
        }

        internal class CloseRoomResponse
        {
            public string status;
        }

        internal class LeaveRoomRespone
        {
            public string status;
        }
        /// <summary>
        /// gets the players in a room
        /// </summary>
        /// <param name="str">a strign with the cuurent users in the room</param>
        public PlayersInRoom(string str)
        {
            InitializeComponent();
            if (str.Split('#').Length == 1)
            {
                this.leaveRoom.Content = "Close Room";
                this.isAdmin = true;
            }
            else
            {
                this.leaveRoom.Content = "Leave Room";
                this.isAdmin = false;
            }
            r.run = true;
            tr = new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                update_thread();
            });
            tr.Start();
        }
        /// <summary>
        /// updates the window checks if ther has been any user that left or joined the room
        /// </summary>
        private void update_thread()
        {
            try
            {
                while(r.run == true)
                {
                    Dispatcher.Invoke(new update_players_callback(update_window));
                    Thread.Sleep(3000);
                    Dispatcher.Invoke(new update_players_callback(deleteTextBlocks));
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// gets an updated list of the userin the room and change 
        /// the users that are shown on the screen if there were changes
        /// </summary>
        private void update_window()
        {
            string msg = "D0002{}";
            serverConnection.sendMsg(msg);
            string format = serverConnection.readMsg(5);
            int len = Convert.ToInt32(format.Substring(1));
            string msgNew = serverConnection.readMsg(len);
            GetRoomStateResponse res = new GetRoomStateResponse();
            try
            {
                res = JsonConvert.DeserializeObject<GetRoomStateResponse>(msgNew);
                this.numOfQuestions = res.answerCount;
                this.timeOut = res.answerTimeOut;
                if (msgNew != "")
                {
                    if (res.hasGameBegun == 2)
                    {
                        StartGameResponse result = new StartGameResponse();
                        msg = "C0002{}";
                        serverConnection.sendMsg(msg);
                        format = serverConnection.readMsg(5);
                        len = Convert.ToInt32(format.Substring(1));
                        msgNew = serverConnection.readMsg(len);
                        result = JsonConvert.DeserializeObject<StartGameResponse>(msgNew);

                        if (result.status == "1")
                        {
                            r.run = false;
                            gamplay win = new gamplay(this.numOfQuestions, this.timeOut);
                            this.Hide();
                            win.Show();
                        }
                        else MessageBox.Show(result.status);
                    }
                    if (res.hasGameBegun == 0 && this.isAdmin == false)
                    {
                        CloseRoomResponse result = new CloseRoomResponse();
                        msg = "E0002{}";
                        serverConnection.sendMsg(msg);
                        format = serverConnection.readMsg(5);
                        len = Convert.ToInt32(format.Substring(1));
                        msg = serverConnection.readMsg(len);
                        result = JsonConvert.DeserializeObject<CloseRoomResponse>(msg);

                        if (result.status == "1")
                        {
                            mainManu win = new mainManu();
                            this.Hide();
                            win.Show();
                        }
                        else MessageBox.Show(result.status);
                    }
                    else
                    {
                        createTextBlocs(res.players);
                        Dispatcher.Invoke(new update_players_callback(addToCanvas));
                        
                    }
                        
                    
                }
            }
            catch(Exception e)
            {
            }

        }
        /// <summary>
        /// creats the text blocs that contains the user names
        /// </summary>
        /// <param name="msgNew"></param>
        private void createTextBlocs(string msgNew)
        {
            var bc = new BrushConverter();
            string[] rr = msgNew.Split('#');
            for (int i = 0; i < rr.Length; i++)
            {
                TextBlock tBlock = new TextBlock();
                if (i == 0)
                {
                    tBlock.Text = "Admin: " + rr[i];
                }
                else
                {
                    tBlock.Text = "User: " + rr[i];
                }
                //tBlock.Foreground = Brushes.White ;
                tBlock.FontSize = 18;
                tBlock.Foreground = (Brush)bc.ConvertFrom("White");
                tBlock.Width = WIDTH;
                tBlock.Height = HEIGHT;
                txb.myTextBlk.Add(tBlock);
            }
        }/// <summary>
        /// deletes the textclocs before updating the window
        /// </summary>
        private void deleteTextBlocks()
        {
            foreach (var item in txb.myTextBlk)
            {

                this.jr.Children.Remove(item);

            }
            txb.myTextBlk.Clear();
        }
        /// <summary>
        /// adds the textblocks to the canvas
        /// </summary>
        private void addToCanvas()
        {
            int i = 0;
            int count = 0;
            STEP = 2;
            currentX = 10;
            currentY = 35;
            foreach (var item in txb.myTextBlk)
            {
                if(count == 0)
                {
                    currentX = 10;
                    i = 0;
                }
                Canvas.SetLeft(item, STEP * currentX);
                Canvas.SetTop(item, STEP * currentY);
                if (i < 3)
                {
                    currentX += 100;
                    i++;
                }
                else
                {
                    currentX = 110;
                    currentY += 30;
                    i = 0;
                }
                count++;
                jr.Children.Add(item);
               
                
            }
        }
        /// <summary>
        /// this function is called when the window is closing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string msg = "60002{}";
            serverConnection.sendMsg(msg);
            string format = serverConnection.readMsg(5);
            int len = Convert.ToInt32(format.Substring(1));
            string msgNew = serverConnection.readMsg(len);
            Environment.Exit(0);
        }
        /// <summary>
        /// return to the main menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GoBack(object sender, RoutedEventArgs e)
        {
            r.run = false;
            if (this.isAdmin == true)
            {
                
                CloseRoomResponse res = new CloseRoomResponse();
                string msg = "B0002{}";
                serverConnection.sendMsg(msg);
                string format = serverConnection.readMsg(5);
                int len = Convert.ToInt32(format.Substring(1));
                string msgNew = serverConnection.readMsg(len);
                res = JsonConvert.DeserializeObject<CloseRoomResponse>(msgNew);
               
                if (res.status == "1")
                {
                    mainManu win = new mainManu();
                    this.Hide();
                    win.Show();

                }
                else MessageBox.Show(res.status);
            }
            else
            {
                LeaveRoomRespone res = new LeaveRoomRespone();
                string msg = "E0002{}";
                serverConnection.sendMsg(msg);
                string format = serverConnection.readMsg(5);
                int len = Convert.ToInt32(format.Substring(1));
                string msgNew = serverConnection.readMsg(len);
                res = JsonConvert.DeserializeObject<LeaveRoomRespone>(msgNew);

                if (res.status == "1")
                {
                    mainManu win = new mainManu();
                    this.Hide();
                    win.Show();

                }
                else MessageBox.Show(res.status);
            }
            this.Hide();
        }
        /// <summary>
        /// starts the game by changing the room
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void startGame_Click(object sender, RoutedEventArgs e)
        {
            string msg = "";
            string format = "" ;
            int len = 0 ;
            string msgNew = "" ;
            StartGameResponse res = new StartGameResponse();
            msg = "C0002{}";
            serverConnection.sendMsg(msg);
            format = serverConnection.readMsg(5);
            len = Convert.ToInt32(format.Substring(1));
            msgNew = serverConnection.readMsg(len);
            res = JsonConvert.DeserializeObject<StartGameResponse>(msgNew);

            if (res.status == "1")
            {
                r.run = false;
                gamplay win = new gamplay(this.numOfQuestions, this.timeOut);
                this.Hide();
                win.Show();
            }
        }
        /// <summary>
        /// leaves the room
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void leaveRoom_Click(object sender, RoutedEventArgs e)
        {
            r.run = false;
            if (this.isAdmin == true)
            {
                CloseRoomResponse res = new CloseRoomResponse();
                string msg = "B0002{}";
                serverConnection.sendMsg(msg);
                string format = serverConnection.readMsg(5);
                int len = Convert.ToInt32(format.Substring(1));
                string msgNew = serverConnection.readMsg(len);
                res = JsonConvert.DeserializeObject<CloseRoomResponse>(msgNew);

                if (res.status == "1")
                {
                    mainManu win = new mainManu();
                    this.Hide();                   
                    win.Show();
                }
                else MessageBox.Show(res.status);
            }
            else
            {
                LeaveRoomRespone res = new LeaveRoomRespone();
                string msg = "E0002{}";
                serverConnection.sendMsg(msg);
                string format = serverConnection.readMsg(5);
                int len = Convert.ToInt32(format.Substring(1));
                string msgNew = serverConnection.readMsg(len);
                res = JsonConvert.DeserializeObject<LeaveRoomRespone>(msgNew);

                if (res.status == "1")
                {
                    mainManu win = new mainManu();
                    this.Hide();
                    win.Show();
                }
                else MessageBox.Show(res.status);
            }
        }
    }


}
