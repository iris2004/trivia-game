﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Timers;
using Newtonsoft.Json;
using System.Windows.Threading;

namespace clientTomer_iris
{
    /// <summary>
    /// Interaction logic for gamplay.xaml
    /// </summary>
    public partial class gamplay : Window
    {
        /// <summary>
        /// contains the paramters of GetQuestionResponse
        /// </summary>
        internal class GetQuestionResponse
        {
            public string status;
            public string question;
            public SortedDictionary<string, string> answers;
        };
        /// <summary>
        /// contains the paramters of SubmitAnswersResponse
        /// </summary>
        internal class SubmitAnswersResponse
        {
            public string status;
            public uint correctAnswerId;

        };

        private uint time = 0;
        private DispatcherTimer t = new DispatcherTimer();
        private uint timeOut;
        private string ansNum;
        SubmitAnswersResponse res;
        private bool submited = false;
        /// <summary>
        /// c'tor gets the game paramters and questions and starts the game
        /// </summary>
        /// <param name="questionQount">the number of questions</param>
        /// <param name="timeOut">the time to answer each question</param>
        public gamplay(uint questionQount, uint timeOut)
        {
            InitializeComponent();
            this.questionsLeft.Text = questionQount.ToString();
            time = timeOut;
            this.timeOut = timeOut;
            getQuestion();
            t.Interval = new TimeSpan(0, 0, 1);
            t.Tick += Timer_Tick;
            t.Start();
        }
        /// <summary>
        /// changes the value of the timer in the game window and checks if the time ran out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer_Tick(object sender, EventArgs e)
        {
            if(time > 0)
            {
                time--;
                this.timer.Text = string.Format("{0}", time % 60);
            }
            else
            {
                if (!submited)
                {
                    res = submitAnswer("0");
                    this.ans1.Background = Brushes.Red;
                    this.ans2.Background = Brushes.Red;
                    this.ans3.Background = Brushes.Red;
                    this.ans4.Background = Brushes.Red;
                }
                submited = false;
                getQuestion();
                time = this.timeOut;
                this.ans1.Background = Brushes.Silver;
                this.ans2.Background = Brushes.Silver;
                this.ans3.Background = Brushes.Silver;
                this.ans4.Background = Brushes.Silver;

            }
        }
        
        /// <summary>
        /// submits answer 1
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ans1_Click(object sender, RoutedEventArgs e)
        {
            ansNum = "1";
            res = submitAnswer(ansNum);
            switchColor(ansNum, res);
            submited = true;
        }
        /// <summary>
        /// submits answer 2
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ans2_Click(object sender, RoutedEventArgs e)
        {
            ansNum = "2";
            res = submitAnswer(ansNum);
            switchColor(ansNum, res);
            submited = true;
        }
        /// <summary>
        /// submits answer 3
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ans3_Click(object sender, RoutedEventArgs e)
        {
            ansNum = "3";
            res = submitAnswer(ansNum);
            switchColor(ansNum, res);
            submited = true;
        }
        /// <summary>
        /// submits answer 4
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ans4_Click(object sender, RoutedEventArgs e)
        {
            ansNum = "4";
            res = submitAnswer(ansNum);
            switchColor(ansNum, res);
            submited = true;
        }
        /// <summary>
        /// get the next question for the user
        /// </summary>
        private void getQuestion()
        {
            bool check = false;
            GetQuestionResponse res = new GetQuestionResponse();
            string msg = "H0002{}";
            this.questionsLeft.Text = (Convert.ToInt32(this.questionsLeft.Text) - 1).ToString();
            if(int.Parse(this.questionsLeft.Text) == -1)
            {
                this.Hide();
                GameRes win = new GameRes();
                win.Show();
                t.Stop();
            }
            else
            {
                while (check != true)
                {
                    serverConnection.sendMsg(msg);
                    string format = serverConnection.readMsg(5);
                    int len = Convert.ToInt32(format.Substring(1));
                    string msgNew = serverConnection.readMsg(len);
                    res = JsonConvert.DeserializeObject<GetQuestionResponse>(msgNew);
                    if(res.status == "1")
                    {
                        check = true;
                    }
                }
                addQuestionAndAnswers(res);
            }
        }
        /// <summary>
        /// adds the option to the game window
        /// </summary>
        /// <param name="res">the response for the getquestionRequest</param>
        private void addQuestionAndAnswers(GetQuestionResponse res)
        {
            this.question.Text = res.question;
            this.ans1.Content = res.answers["0"];
            this.ans2.Content = res.answers["1"];
            this.ans3.Content = res.answers["2"];
            this.ans4.Content = res.answers["3"];
        }
        /// <summary>
        /// submits the user answer
        /// </summary>
        /// <param name="ansNum">the option the user chose number</param>
        /// <returns>the response for the request </returns>
        private SubmitAnswersResponse submitAnswer(string ansNum)
        {
            
            ansNum = (Convert.ToInt32(ansNum) - 1).ToString();
            string msg = "{\"answerid\":"+ ansNum + ", \"answerTime\":" + Convert.ToString(timeOut - Convert.ToInt32(this.time)) + "}";
            msg = "G" + Format.lenFormat(msg.Length) + msg;
            serverConnection.sendMsg(msg);
            string format = serverConnection.readMsg(5);
            int len = Convert.ToInt32(format.Substring(1));
            string msgNew = serverConnection.readMsg(len);
            SubmitAnswersResponse res = JsonConvert.DeserializeObject<SubmitAnswersResponse>(msgNew);
            return res;
        }
        /// <summary>
        /// switches the color of the buttons depends on the user answer
        /// </summary>
        /// <param name="ansNum">the option the user chose number </param>
        /// <param name="res">conatins the id of the correct answer</param>
        private void switchColor(string ansNum, SubmitAnswersResponse res)
        {
            switch (res.correctAnswerId)
            {
                case 1:
                    this.ans1.Background = Brushes.Green;                    
                    break;
                case 2:
                    this.ans2.Background = Brushes.Green;
                    break;
                case 3:
                    this.ans3.Background = Brushes.Green;
                    break;
                case 4:
                    this.ans4.Background = Brushes.Green;
                    break;
            }

            if (ansNum != (res.correctAnswerId).ToString())
            {
                switch (ansNum)
                {
                    case "1":
                        this.ans1.Background = Brushes.Red;
                        break;
                    case "2":
                        this.ans2.Background = Brushes.Red;
                        break;
                    case "3":
                        this.ans3.Background = Brushes.Red;
                        break;
                    case "4":
                        this.ans4.Background = Brushes.Red;
                        break;
                }

            }

           
        }
        /// <summary>
        /// the function that is called when the page closes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string msg = "60002{}";
            serverConnection.sendMsg(msg);
            string format = serverConnection.readMsg(5);
            int len = Convert.ToInt32(format.Substring(1));
            string msgNew = serverConnection.readMsg(len);
            Environment.Exit(0);

        }

    }
}
