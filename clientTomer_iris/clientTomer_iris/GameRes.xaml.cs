﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace clientTomer_iris
{
    /// <summary>
    /// Interaction logic for GameRes.xaml
    /// </summary>
    public partial class GameRes : Window
    {
        /// <summary>
        /// contains the paramters of PlayerResultResponse
        /// </summary>
        internal class PlayerResultResponse
        {
            public string userName;
            public uint correctAnswerCount;
            public uint wrongtAnswerCount;
            public uint AverageAnswerTime;

        };
        /// <summary>
        /// contains the paramters of LeaveGameResponse
        /// </summary>
        internal class LeaveGameResponse
        {
            public uint status;
        };
        /// <summary>
        /// contains the paramters of GetGameResultResponse
        /// </summary>
        internal class GetGameResultResponse
        {
            public uint status;
            public string results;
        };
        private Dictionary<string, string>[] dict;
        /// <summary>
        /// c'tor request the game results
        /// </summary>
        public GameRes()
        {
            InitializeComponent();
            string msg = "F0002{}";
            serverConnection.sendMsg(msg);
            string format = serverConnection.readMsg(5);
            int len = Convert.ToInt32(format.Substring(1));
            string msgNew = serverConnection.readMsg(len);
            GetGameResultResponse res = JsonConvert.DeserializeObject<GetGameResultResponse>(msgNew);
            string[] rr = res.results.Split("#");
            dict = new Dictionary<string, string>[rr.Length];
            for (int i = 0; i < rr.Length; i++)
            {
                dict[i] = rr[i].Split(',').Select(x => x.Split(':')).ToDictionary(x => x[0], x => x[1]);
            }
            for (int i = 0; i < dict.Length; i++)
            {
                if (dict[i]["userName"] == userName.name)
                {
                    this.UserName.Text += "UserName:" + dict[i]["userName"];
                    this.correctAnswerCount.Text += "correctAnswerCount:" + dict[i]["correctAnswerCount"];
                    this.wrongtAnswerCount.Text += "wrongAnswerCount:" + dict[i]["wrongAnswerCount"];
                    this.AverageAnswerTime.Text += "AverageAnswerTime:" + dict[i]["AverageAnswerTime"];
                }
            }
            
        }
        /// <summary>
        /// goes back to the main menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GoBack(object sender, RoutedEventArgs e)
        {
            mainManu win = new mainManu();
            win.Show();
            this.Hide();
        }
        /// <summary>
        /// the function that is called when you close the window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string msg = "60002{}";
            serverConnection.sendMsg(msg);
            string format = serverConnection.readMsg(5);
            int len = Convert.ToInt32(format.Substring(1));
            string msgNew = serverConnection.readMsg(len);
            Environment.Exit(0);

        }
    }
}
