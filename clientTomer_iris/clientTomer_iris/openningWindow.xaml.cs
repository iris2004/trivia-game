﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Net;

namespace clientTomer_iris
{
    static class serverConnection
    {
        static private NetworkStream clientStream;
        /// <summary>
        /// connect to the server
        /// </summary>
        static public void connect()
        {
            TcpClient client = new TcpClient();
            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8876);
            client.Connect(serverEndPoint);
            clientStream = client.GetStream();
        }
        /// <summary>
        /// send the messege to the server
        /// </summary>
        /// <param name="msg"></param>
        static public void sendMsg(string msg)
        {
            byte[] buffer = new ASCIIEncoding().GetBytes(msg);
            clientStream.Write(buffer, 0, buffer.Length);
            clientStream.Flush();
        }
        /// <summary>
        /// reads the msg the server sent
        /// </summary>
        /// <param name="len"></param>
        /// <returns></returns>
        static public string readMsg(int len)
        {
            byte[] buffer = new byte[len];
            int bytesRead;
            if (len != 0)
            {
                bytesRead = clientStream.Read(buffer, 0, len);
                return Encoding.Default.GetString(buffer);
            }
            return "";
        }
        /// <summary>
        /// gets the clientStream
        /// </summary>
        /// <returns></returns>
        static public NetworkStream getClientStream()
        {
            return clientStream;
        }
    }
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        /// <summary>
        /// c'tor
        /// </summary>
        public Window1()
        {
            serverConnection.connect();
            InitializeComponent();
        }
        /// <summary>
        /// goes to the sigup window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cSignUp(object sender, RoutedEventArgs e)
        {
            SignUp win2 = new SignUp();
            win2.Show();
            this.Close();
        }
        /// <summary>
        /// goes to the login window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cLogin(object sender, RoutedEventArgs e)
        {
            Login win2 = new Login();
            win2.Show();
            this.Close();
        }
    }
}
