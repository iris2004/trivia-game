﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;
/// <summary>
/// this class tells the thread when to stop
/// </summary>
public class Run
{
    public bool run;
}
/// <summary>
/// contains the the buttons the will be used for the join room request
/// </summary>
static public class btns
{
    static public List<Button> myBtns = new List<Button>();
};

namespace clientTomer_iris
{
    /// <summary>
    /// Interaction logic for JoinRoom.xaml
    /// </summary>
    /// <summary>
    /// contains the paramters of GetRoomStateResponse
    /// </summary>
    public class GetRoomStateResponse
    {
        public uint status;
        public uint hasGameBegun;
        public string players;
        public uint answerCount;
        public uint answerTimeOut;
    };

    public partial class JoinRoom : Window
    {
        delegate void update_rooms_callback();
        private Thread tr;
        private Run r = new Run();


        private Dictionary<string, string>[] dict;
        private uint HEIGHT;
        private uint WIDTH;
        private uint STEP;
        private int currentX;
        private int currentY;
        /// <summary>
        /// contains the paramters of getPlayersInARoomReq
        /// </summary>
        public class getPlayersInARoomReq
        {
            public int roomId;
        }
        /// <summary>
        /// contains the paramters of getPlayersInARoomRes
        /// </summary>
        public class getPlayersInARoomRes
        {
            public string users;
        }
        /// <summary>
        /// contains the paramters of joinRoomReq
        /// </summary>
        public class joinRoomReq
        {
            public int roomId;
        }
        /// <summary>
        /// contains the paramters of joinRoomResponse
        /// </summary>
        public class joinRoomResponse
        {
            public string status;
        }
       /// <summary>
       /// c'tor starts the thread
       /// </summary>
        public JoinRoom()
        {
            InitializeComponent();           
            r.run = true;
            tr = new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                update_thread();
            });
            tr.Start();
        }
        /// <summary>
        /// creats the buttons froom the rooms that the user requested
        /// </summary>
        /// <param name="msgNew">the respons from the server befor parsing</param>
        private void CreateButtons(string msgNew)
        {
            var bc = new BrushConverter();
            string[] rr = msgNew.Split(',');
            this.dict = new Dictionary<string, string>[rr.Length];
            HEIGHT = 40;
            WIDTH = 100;
            STEP = 2;
            currentX = 10;
            currentY = 60;
            if (rr[0] != "" && msgNew != "{\"status\":\"request is irelevant\"}")
            {
                for (int i = 0; i < rr.Length; i++)
                {
                    dict[i] = rr[i].Split('#').Select(x => x.Split('=')).ToDictionary(x => x[0], x => x[1]);
                    Button btnNew = new Button();
                    btnNew.Content = this.dict[i]["name"];
                    btnNew.Name = "a" + this.dict[i]["id"];
                    btnNew.Width = WIDTH;
                    btnNew.Height = HEIGHT;
                    btnNew.Click += Cjoin;
                    btnNew.Foreground = Brushes.LightGray;
                    btnNew.Background = Brushes.RosyBrown;
                    btnNew.BorderBrush = Brushes.Black;
                    btnNew.BorderThickness = new Thickness(3);
                    btnNew.FontSize = 20;
                    btns.myBtns.Add(btnNew);
                }
            }
        }/// <summary>
        /// add the buttons to the window
        /// </summary>
        private void addToCanvas()
        {
            List<Button> myBtns = btns.myBtns;
            int i = 0;
            foreach (var item in myBtns)
            {

                Canvas.SetLeft(item, STEP * currentX);
                Canvas.SetTop(item, STEP * currentY);
                if (i < 3)
                {
                    currentX += 85;
                    i++;
                }
                else
                {
                    currentX = 10;
                    currentY += 30;
                    i = 0;
                }
                this.can.Children.Add(item);

            }
        }
        /// <summary>
        /// called when the window is closed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string msg = "60002{}";
            serverConnection.sendMsg(msg);
            string format = serverConnection.readMsg(5);
            int len = Convert.ToInt32(format.Substring(1));
            string msgNew = serverConnection.readMsg(len);
            Environment.Exit(0);

        }
        /// <summary>
        /// send a join request for a room of the user choice
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cjoin(object sender, RoutedEventArgs e)
        {
            r.run = false;
            joinRoomReq req = new joinRoomReq();
            req.roomId = Convert.ToInt32(((Button)sender).Name.Substring(1));
            string msg = JsonConvert.SerializeObject(req);
            string newmsg = "4" + Format.lenFormat(msg.Length) + msg;
            serverConnection.sendMsg(newmsg);
            string format = serverConnection.readMsg(5);
            int len = Convert.ToInt32(format.Substring(1));

            string msgNew = serverConnection.readMsg(len);
            joinRoomResponse res = new joinRoomResponse();
            res = JsonConvert.DeserializeObject<joinRoomResponse>(msgNew);


            getPlayersInARoomFunc(res, sender);

        }
        /// <summary>
        /// gets all the players that are in the room
        /// </summary>
        /// <param name="res">the respons of the server to the join room request </param>
        /// <param name="sender">the button of the room the user wants to join</param>
        public void getPlayersInARoomFunc(joinRoomResponse res, Object sender)
        {
            if (res.status == "1")
            {
                
                string newmsg = "D0002{}";
                serverConnection.sendMsg(newmsg);
                string format = serverConnection.readMsg(5);
                int len = Convert.ToInt32(format.Substring(1));
                string msgNew = serverConnection.readMsg(len);
                GetRoomStateResponse result = new GetRoomStateResponse();

                result = JsonConvert.DeserializeObject<GetRoomStateResponse>(msgNew);
                PlayersInRoom win = new PlayersInRoom(result.players);
                win.Show();
                this.Hide();
                
            }
            else
            {
                MessageBox.Show(res.status);
            }
        }
        /// <summary>
        /// takes the user back to the main menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GoBack(object sender, RoutedEventArgs e)
        {
            mainManu win = new mainManu();
            r.run = false;
            win.Show();
            this.Hide();
        }
        /// <summary>
        /// delets the button on the window
        /// </summary>
        private void deleteButtons()
        {
            foreach (var item in btns.myBtns)
            {

                this.can.Children.Remove(item);

            }
            btns.myBtns.Clear();
        }
        /// <summary>
        /// upadts the screen and the check if a room was added
        /// </summary>
        private void update_thread()
        {
            try
            {
                Thread.Sleep(500);
                while(r.run == true)
                {
                    int i = 0;
                    int delay_ms = 3000;
                    Dispatcher.Invoke(new update_rooms_callback(update_window));
                    Thread.Sleep(delay_ms);
                    Dispatcher.Invoke(new update_rooms_callback(deleteButtons));
                }
                
                
            }
            catch (ThreadAbortException ex)
            {
                MessageBox.Show(ex.Message);
               
            }
        }
        /// <summary>
        /// requests from the server all the availible rooms 
        /// </summary>
        private void update_window()
        {
            string msg = "70002{}";
            serverConnection.sendMsg(msg);
            string format = serverConnection.readMsg(5);
            int len = Convert.ToInt32(format.Substring(1));

            string msgNew = serverConnection.readMsg(len);

            if (msgNew != "")
            {
                CreateButtons(msgNew);
                Dispatcher.Invoke(new update_rooms_callback(addToCanvas));
            }
        }
    }
}
