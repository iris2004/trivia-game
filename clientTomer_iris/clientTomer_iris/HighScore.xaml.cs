﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;
using System.Threading;
namespace clientTomer_iris
{
    /// <summary>
    /// Interaction logic for HighScore.xaml
    /// </summary>
    public partial class HighScore : Window
    {
        /// <summary>
        /// contains the paramters of HighScoresResponse
        /// </summary>
        internal class HighScoresResponse
        {
            public string status;
            public string statistics;
            
        }
        delegate void updateHighScore();
        Run r = new Run();
        Thread tr;
        /// <summary>
        /// c'tor starts the threads
        /// </summary>
        public HighScore()
        {
            
            InitializeComponent();
            r.run = true;
            tr = new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                update_thread();
            });
            tr.Start();
        }
        /// <summary>
        /// the function that is called when the user closes the window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Quit_click(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string msg = "60002{}";
            serverConnection.sendMsg(msg);
            string format = serverConnection.readMsg(5);
            int len = Convert.ToInt32(format.Substring(1));
            string msgNew = serverConnection.readMsg(len);
            Environment.Exit(0);
        }
        /// <summary>
        /// goes back to the main menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GoBack(object sender, RoutedEventArgs e)
        {
            r.run = false;
            Statistics win = new Statistics();
            win.Show();
            this.Hide();
        }
        /// <summary>
        /// the thread that updats the window every 3 seconeds
        /// </summary>
        private void update_thread()
        {
            try
            {
                Thread.Sleep(500);
                while (r.run == true)
                {
                    int delay_ms = 3000;
                    Dispatcher.Invoke(new updateHighScore(update_window));
                    Thread.Sleep(delay_ms);
                    
                }


            }
            catch (ThreadAbortException ex)
            {
                MessageBox.Show(ex.Message);

            }
        }
        /// <summary>
        /// request the top three users
        /// called by the thread to update and check if a user was changed
        /// </summary>
        private void update_window()
        {
            HighScoresResponse res = new HighScoresResponse();
            string msg = "90002{}";
            serverConnection.sendMsg(msg);
            int len = Convert.ToInt32(serverConnection.readMsg(5).Substring(1));
            msg = serverConnection.readMsg(len);
            res = JsonConvert.DeserializeObject<HighScoresResponse>(msg);
            if (res.status == "1")
            {
                string[] users = res.statistics.Split(",");
                this.tUser1.Text = users[0];
                this.tUser2.Text = users[1];
                this.tUser3.Text = users[2];
            }
            else
            {
                MessageBox.Show(res.status);
            }
        }
    }
}
