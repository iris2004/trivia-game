﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace clientTomer_iris
{
    /// <summary>
    /// Interaction logic for Statistics.xaml
    /// </summary>
    public partial class Statistics : Window
    {
        /// <summary>
        /// c;tor
        /// </summary>
        public Statistics()
        {
            InitializeComponent();
        }
        /// <summary>
        /// gets the user personal stats
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PersonalStats_click(object sender, RoutedEventArgs e)
        {
            UserStatistics win2 = new UserStatistics();
            win2.Show();
            this.Hide();
        }
        /// <summary>
        /// gets the highest scoring users
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HighScores_click(object sender, RoutedEventArgs e)
        {
            HighScore win2 = new HighScore();
            win2.Show();
            this.Hide();
        }
        /// <summary>
        /// called when the window is closing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Quit_click(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string msg = "60002{}";
            serverConnection.sendMsg(msg);
            string format = serverConnection.readMsg(5);
            int len = Convert.ToInt32(format.Substring(1));
            string msgNew = serverConnection.readMsg(len);
            Environment.Exit(0);
        }
        //goes back to the main menu
        private void GoBack(object sender, RoutedEventArgs e)
        {
            mainManu win = new mainManu();
            win.Show();
            this.Hide();
        }
    }
}
