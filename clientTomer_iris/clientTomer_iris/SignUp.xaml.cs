﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace clientTomer_iris
{
    /// <summary>
    /// formats the messeges befor sending them to the server
    /// </summary>
    static class Format
    {
        public static string lenFormat(int len)
        {
            string lenFormat = "";
            for (int i = 0; i < 4 - Convert.ToString(len).Length; i++)
            {
                lenFormat = '0' + lenFormat;
            }
            lenFormat += Convert.ToString(len);
            return lenFormat;
        }
    }
    /// <summary>
    /// Interaction logic for SignUp.xaml
    /// </summary>
    public partial class SignUp : Window
    {
        public class SignUpRequest
        {
            public string username;
            public string password;
            public string email;
        }
        public class SignUpResponse
        {
            public string status;
        }

        public SignUp()
        {
            InitializeComponent();
        }
        /// <summary>
        /// sens a signup request
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CSend(object sender, RoutedEventArgs e)
        {
            SignUpRequest req = new SignUpRequest();
            req.username = this.tUserName.Text;
            req.password = this.tPassword.Text;
            req.email = this.tEmail.Text;

            string msg = JsonConvert.SerializeObject(req);
            string fullmsg = "2" + Format.lenFormat(msg.Length) + msg;
            serverConnection.sendMsg(fullmsg);
            req = JsonConvert.DeserializeObject<SignUpRequest>(msg);

            string format = serverConnection.readMsg(5);
            int len = Convert.ToInt32(format.Substring(1));    

            string msgNew = serverConnection.readMsg(len);
            SignUpResponse res = new SignUpResponse();
            res = JsonConvert.DeserializeObject<SignUpResponse>(msgNew);

            if (res.status == "1")
            {
                Login win2 = new Login();
                win2.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show(res.status);
                SignUp win2 = new SignUp();
                win2.Show();
                this.Close();
            }
        }
        /// <summary>
        /// go back to the welcome
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GoBack(object sender, RoutedEventArgs e)
        {
            Window1 win = new Window1();
            win.Show();
            this.Hide();
        }
    }
}
