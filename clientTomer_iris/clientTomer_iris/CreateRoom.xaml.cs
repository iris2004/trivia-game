﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace clientTomer_iris
{
    /// <summary>
    /// Interaction logic for CreateRoom.xaml
    /// </summary>
    
    public partial class CreateRoom : Window
    {
        /// <summary>
        /// contains the paramters of creatRoomRequest
        /// </summary>
        internal class CreateRoomRequest
        {
            public string roomName;
            public int questionCount;
            public int maxUsers;
            public int answerTimeOut;
        }
        /// <summary>
        /// contains the paramters of creatRoomResponse
        /// </summary>
        internal class CreateRoomResponse
        {
            public string status;
        }
        /// <summary>
        /// contains the paramters of getPlayersInARoomReq
        /// </summary>
        public class getPlayersInARoomReq
        {
            public int roomId;
        }
        /// <summary>
        /// contains the paramters of getPlayersInARoomRes
        /// </summary>
        public class getPlayersInARoomRes
        {
            public string users;
        }
        /// <summary>
        /// c'tor
        /// </summary>
        public CreateRoom()
        {
            InitializeComponent();
        }
        /// <summary>
        /// this is the function that is called when the window closes
        /// </summary>
        /// <param name="sender">the button w</param>
        /// <param name="e">the event value</param>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string msg = "60002{}";
            serverConnection.sendMsg(msg);
            string format = serverConnection.readMsg(5);
            int len = Convert.ToInt32(format.Substring(1));
            string msgNew = serverConnection.readMsg(len);
            Environment.Exit(0);

        }
        /// <summary>
        /// creats a room 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void createRoom_Click(object sender, RoutedEventArgs e)
        {
            CreateRoomRequest req = new CreateRoomRequest();
            CreateRoomResponse res = new CreateRoomResponse();
            req.roomName = this.tRoomName.Text;
            bool flag = true;
            try
            {
                req.questionCount = Convert.ToInt32(this.tQuestionCount.Text);
                req.maxUsers = Convert.ToInt32(this.tMaxUsers.Text);
                req.answerTimeOut = Convert.ToInt32(this.tAnswerTimeOut.Text);
            }
            catch(Exception ex)
            {
                flag = false;
                MessageBox.Show("Invalid Input");
               

            }
            if (flag)
            {
                string msg = JsonConvert.SerializeObject(req);
                msg = "5" + Format.lenFormat(msg.Length) + msg;
                serverConnection.sendMsg(msg);

                int len = Convert.ToInt32(serverConnection.readMsg(5).Substring(1));
                string msgNew = serverConnection.readMsg(len);
                res = JsonConvert.DeserializeObject<CreateRoomResponse>(msgNew);
                if (res.status == "1")
                {
                    //MessageBox.Show("room created successfully");
                    getPlayersInARoomReq rq = new getPlayersInARoomReq();
                    PlayersInRoom win = new PlayersInRoom(userName.name);
                    win.Show();
                    this.Hide();

                }
                else
                {
                    MessageBox.Show(res.status);
                }
            }

        }
        /// <summary>
        /// goes to the main menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GoBack(object sender, RoutedEventArgs e)
        {
            mainManu win = new mainManu();
            win.Show();
            this.Hide();
        }

        private void tRoomName_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }

}
