﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace clientTomer_iris
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    /// <summary>
    /// saves the user userName for later use
    /// </summary>
    public static class userName
    {
        public static string name;
    }
    public partial class Login : Window
    {
        public class LoginRequest
        {
            public string username;
            public string password;

        }
        public class LoginResponse
        {
            public string status;
        }
        /// <summary>
        /// c'tor
        /// </summary>
        public Login()
        {
            InitializeComponent();
        }
        /// <summary>
        /// send the user login requests
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cSend(object sender, RoutedEventArgs e)
        {
            LoginRequest req = new LoginRequest();
            req.username = this.tUserName.Text;
            req.password = this.tPassword.Text;
           

            string msg = JsonConvert.SerializeObject(req);

            string fullmsg = "1" + Format.lenFormat(msg.Length) + msg;
            serverConnection.sendMsg(fullmsg);
            req = JsonConvert.DeserializeObject<LoginRequest>(msg);

            string format = serverConnection.readMsg(5);
            int len = Convert.ToInt32(format.Substring(1));

            string msgNew = serverConnection.readMsg(len);
            LoginResponse res = new LoginResponse();
            res = JsonConvert.DeserializeObject<LoginResponse>(msgNew);


            if (res.status == "1")
            {
                userName.name = req.username;
                mainManu win2 = new mainManu();
                win2.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show(res.status);
            }

        }
        /// <summary>
        /// goes back to the welcom window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GoBack(object sender, RoutedEventArgs e)
        {
            Window1 win = new Window1();
            win.Show();
            this.Hide();
        }

        private void tUserName_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }


}
