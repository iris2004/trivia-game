﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace clientTomer_iris
{
    /// <summary>
    /// Interaction logic for mainManu.xaml
    /// </summary>
    public partial class mainManu : Window
    {
        /// <summary>
        /// c'tor
        /// </summary>
        public mainManu()
        {
            InitializeComponent();
        }
        /// <summary>
        /// exits the game
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            Environment.Exit(0);
        }
        /// <summary>
        /// goes to the statistics window;
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Statistics_Click(object sender, RoutedEventArgs e)
        {
            Statistics win = new Statistics();
            win.Show();
            this.Hide();
        }
        /// <summary>
        /// goes to the join room window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void JoinRoom_Click(object sender, RoutedEventArgs e)
        {
            JoinRoom win = new JoinRoom();
            win.Show();
            this.Hide();
        }
        /// <summary>
        /// goes to the Creat room window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateRoom_Click(object sender, RoutedEventArgs e)
        {
            CreateRoom win = new CreateRoom();
            win.Show();
            this.Hide();
        }
        
        /// <summary>
        /// when the window closes this function is called
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Quit_click(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string msg = "60002{}";
            serverConnection.sendMsg(msg);
            string format = serverConnection.readMsg(5);
            int len = Convert.ToInt32(format.Substring(1));
            string msgNew = serverConnection.readMsg(len);
            Environment.Exit(0);

        }
    }

}
